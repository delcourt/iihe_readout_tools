# CMS-MUonE Beam Test Environment monitoring documentation

## Introduction
This document concentrates information relating to the CMS-MUonE beam test environment monitoring system.

The setup is comprised of three main components :
*   The temperature and humidity "Rh/T" monitoring system and related software
*   The Keithley and Hameg power supply monitoring and control tool
*   The web interface displaying monitoring information and, possibly, DQM plots

All softwares are deployed in `/home/xtaldaq/Env_monitoring/`.

## "Rh/T" Environment monitoring


### Physical setup overview

A detailled description of the hardware can be found [at the following link](https://twiki.cern.ch/twiki/bin/viewauth/Main/BTEnvMonitor). 

A schematic overview of the hardware was presented [here](https://indico.cern.ch/event/1051372/contributions/4462454/attachments/2289467/3892135/210729_MT_intro_monitoring.pdf), on slides #6-7.

### Flashing the firmware

TODO

### Connections
The following pictures shows the main Rh/T board fully connected :

![Main board](main_board.JPG)

Two USB cables are connected to the µC development board. The top one (µUSB type B), provides power and a serial connection to ph2acf-01, allowing to read-out monitoring data from the board. The bottom one (USB type A) is connected to a programmer and allows to flash the firmware if required, and can serve as a power source. This later cable is not required if the top cable is connected.

On the right hand side, two RJ45 connectors allow to reach multiplexer board. The connector for each multiplexer used is important, since it will impact sensor naming offline.

DO NOT CONNECT THE RJ45 CONNECTORS TO ANY OTHER APPLIANCE. IT IS NOT ETHERNET PROTOCOL AND THINGS MIGHT BREAK.

The following picture shows a disconnected multiplexer board.

![Mux board](mux_board.JPG)

The RJ45 connector can be connected to either main board connectors. Beware that the order matters for naming.

Two other types of connectors are available. The four-pins connectors allow for I2C communication (will be called I2C connectors); and the two-pins connectors allow analogue signals to be sent to the µC (will be called analogue connectors).

To the right of the board, four I2C connectors can be seen. These are connected to the multiplexer, and thus allow connecting HIH sensors that all have the same address. The naming offline will depend on the slot used. A small number is shown next to the connectors, with the first slot being on the bottom left of this picture, second to the bottom right, third top right, fourth top left. 

To the left of the board, two I2C connectors can be found. They are directly linked to the main I2C lines, and allow to daisy-chain multiple multiplexer boards if needed. THIS WILL REQUIRE A FIRMWARE PATCH.

Finally, the four analogue connectors can be used for PT sensors. If you want to use these, it is advised to have the four connected for better measurements.

### Configuration

To run the software, a configuration file is required. Such a file can be [seen here](conf_batch.json). For the end-user, the following parameters are important : 

#### `alias` 
This contains a dictionary of user friendly names attached to the default, hardware-based naming. 
The hardware naming goes as follow : 

*   `HIH_0_[T/H]` : Ethernet slot #0, I2C connector #0 or #4 (numbers shown depend on board)
*   `HIH_1_[T/H]` : Ethernet slot #0, I2C connector #1 or #5 (numbers shown depend on board)
*   `HIH_2_[T/H]` : Ethernet slot #0, I2C connector #2 or #6 (numbers shown depend on board)
*   `HIH_3_[T/H]` : Ethernet slot #0, I2C connector #3 or #7 (numbers shown depend on board)
*   `HIH_4_[T/H]` : Ethernet slot #1, I2C connector #0 or #4 (numbers shown depend on board)
*   `HIH_5_[T/H]` : Ethernet slot #1, I2C connector #1 or #5 (numbers shown depend on board)
*   `HIH_6_[T/H]` : Ethernet slot #1, I2C connector #2 or #6 (numbers shown depend on board)
*   `HIH_7_[T/H]` : Ethernet slot #1, I2C connector #3 or #7 (numbers shown depend on board)
*   `PT_100_[0-3]` : Ethernet slot #0, Analogue connector #0-3 or #4-7
*   `PT_100_[4-7]` : Ethernet slot #1, Analogue connector #0-3 or #4-7

For the `HIH` detectors, a name in `_T` is a temperature measurement, and in `_H` is a humidity measurement.

#### `meas_to_show`

Defines the list of measurements to show on the monitoring software or the web interface.

#### `save_file`
Defines the name of the `.csv` file produced. This has to be changed everytime connections are changed.

#### `dump_file`
Defines the name of the `.bin` raw data file produced. This file is the one used by the web interface. It is also adviced to have different names for different hardware connections.

#### `port`
Defines the serial port used to communicate with the µC. Be advised that this port can change if other serial connections are set-up. This can for example be the case if the "programmer" USB cable is connected to the µC.

### Logging software

For this beam test, since no graphical interface is foreseen, the following script `/home/xtaldaq/Env_monitoring/magnet-test-env-monitoring/batch_runner.py` can be used to store data from the µC.

From the `/home/xtaldaq/Env_monitoring/magnet-test-env-monitoring` folder, run : 

```bash
python3 batch_runner.py
```

This will launch an infinite loop, reading whatever is to be read. It is advised to run it in a tmux terminal. At the time of writing, it is running in tmux terminal #1 (`tmux a -t 1` to attach; `CTRL-b d` to detach).

## Power supply control

### Logging 
Power supplies are logged using the `/home/xtaldaq/Env_monitoring/psu_monitoring/logger.py` script.

From the `/home/xtaldaq/Env_monitoring/psu_monitoring` folder, run :

```bash
python3 logger.py
```

This will launch an infinite loop, reading data every ten seconds. It is advised to run it in a tmux terminal. At the time of writing, it is running in tmux terminal #0 (`tmux a -t 0` to attach; `CTRL-b d` to detach).

### Control
#### R&S HMP4040 
It is possible to turn ON/OFF channels of the LV power supply using the `/home/xtaldaq/Env_monitoring/psu_monitoring/HMP4040.py` script.

From the `/home/xtaldaq/Env_monitoring/psu_monitoring` folder, run :

```bash
python3 HMP4040.py
```

This start a small interactive script allowing you to choose which channel to act upon and what to do with it.

#### Keithley 
It is possible to turn ON/OFF HV power supplies and ramp them using the `/home/xtaldaq/Env_monitoring/psu_monitoring/keithley.py` script.

From the `/home/xtaldaq/Env_monitoring/psu_monitoring` folder, run :

```bash
python3 keithley.py
```

This start a small interactive script. First choice is the HV PSU. The "CMS" PSU has a "CMS" sticker and the "Croc" PSU has a small sticker on the from pannel with a crocodile telling you not to use it for sensitive equipment.

Then you can choose either,
- `read` to get the voltages and current of the supply
- `power on` to turn on the channel. WARNING! ONLY DO THIS IF 0V IS SET! NO RAMP IS IMPLEMENTED HERE!
- `power off` to turn off the channel. WARNING! ONLY DO THIS IF 0V IS SET! NO RAMP IS IMPLEMENTED HERE!
- `ramp` opens a drop-down menu to gently increase/decrease to voltage. You can choose `step` to define the step of each ramp; `u` or `up` to increase bias; `d` or `down` to decrease the bias. `read` will update monitoring values and `quit` will quit this menu.

### Comments 

-  There's a file based lock system that prevents multiple scripts to access the PSU at the same time. If you have repeated "Waiting..." being printed, you should remove them (it's ".hmp" and ".keithley" in that folder)
-  I wrote down the serial numbers of the PSU in the scripts. If you change the cables, it should work if you don't have this "hanging while listing devices" issue. If that is the case, you have to either call me, or edit the ´BLACK_LIST´ list at the beginning of ´psu_common.py´ to contain any connection that timesout (the one with an error being printed, not the one hanging)


## Web interface

A web interface is running on [http://ph2acf-01/](http://ph2acf-01/), accessible from within the CERN network. Note the absence of `s` in `http`.

To start the web interface, from the `/home/xtaldaq/Env_monitoring/ph2_mt_dqm/python/web_server` folder, run :

```bash
sudo ./runner.sh
```

The super-user privileges are required to open the correct port.

It is advised to run it in a tmux terminal. At the time of writing, it is running in tmux terminal #2 (`tmux a -t 2` to attach; `CTRL-b d` to detach).

import socket,time, os, json, datetime

refresh = 30
CMD_DELAY = 0.1

# Opening JSON file
f = open('ESP32_calib.json')
converter = json.load(f)

min_conv = 999
max_conv = 0
for entry in converter:
    entry  = float(entry)
    if entry < min_conv:
        min_conv = entry
    if entry > max_conv:
        max_conv = entry

def get_charge(volt):
    if volt < min_conv:
        return 0
    if volt > max_conv:
        return 100
    if str(volt) not in converter.keys():
        return 0
    return float(converter[str(volt)])



def get_message(s):
    msg = ""
    while msg == "":
        msg = s.recv(512).decode("utf-8").replace("\n","").replace("\r","")
    return msg

def send_message(s,msg):
    s.send(bytearray(msg+"\n","utf-8"))


s = None
connected = False
last_success = time.time()
while True:
    # print("Trying to connect...")
    try:
        s = socket.socket(socket.AF_BLUETOOTH, socket.SOCK_STREAM, socket.BTPROTO_RFCOMM)
        s.connect(('E0:E2:E6:76:16:8E', 1))
        established = False
        while established == False:
            values = get_message(s)
            if len(values.split(",")) == 4:
                send_message(s,"Ack")
                data = (str(time.time())+","+values)
                vs = values.split(",")
                print(f"{datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')} : {vs[0]}C; {vs[1]}% hum; {get_charge(float(vs[2])):.1f}% bat; {vs[3]} broadcasts")
                with open("humidity.csv","a") as f:
                    f.write(data+"\n")
            established = True
        s.close()
        # print(f"Sleeping for {refresh+1.5}s")
        last_success = time.time()
        time.sleep(refresh-1)
    except Exception as e:
        print(f"{datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')} : Cannot connect: {e}")
        s.close()
        if time.time() - last_success > 4*refresh:
            print(f"{datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')} : No data for {time.time() - last_success}s, restarting the bluetooth service...")
            os.system("bluetoothctl power off")
            time.sleep(10)
            os.system("bluetoothctl power on")
            last_success = time.time()
            
        time.sleep(CMD_DELAY)

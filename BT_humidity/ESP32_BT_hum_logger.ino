#include <BluetoothSerial.h>

#include "DHT.h"
#define DHTPIN 4     
#define DHTTYPE DHT22 

unsigned long silence_start;
#define uS_TO_S_FACTOR 1000000
#define SLEEP_TIME 30 



#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

#if !defined(CONFIG_BT_SPP_ENABLED)
#error Serial Bluetooth not available or not enabled. It is only available for the ESP32 chip.
#endif

BluetoothSerial SerialBT;
RTC_DATA_ATTR int bootCount = 0;




DHT dht(DHTPIN, DHTTYPE);

void send_message(String message){
  int   ArrayLength  = message.length()+1;    //The +1 is for the 0x00h Terminator
  char  CharArray[ArrayLength];
  message.toCharArray(CharArray,ArrayLength);

  SerialBT.println(message);
  Serial.println(message);
}

void setup() {
  Serial.begin(115200);
  Serial.println("Booting up... Boot #"+String(bootCount));
  delay(10);
  dht.begin();
  

  SerialBT.begin("IIHE_BT_LOGGER");

  float h = dht.readHumidity();
  float t = dht.readTemperature();
  int loop_breaker = 0;
  while (isnan(h)) {
    dht.begin();
    delay(50);
    h = dht.readHumidity();
    t = dht.readTemperature();
    Serial.print(String(h)+"\n");       
    loop_breaker++;
    if (loop_breaker > 10){
      Serial.println("Unable to readout data...");
      h = -999;
      t = -999;
    }        
  }
  float b=analogRead(35)/4096.0*7.445; //Battery monitoring
  String message = String(t)+","+String(h)+","+String(b);


  int n_iter_max = 20;
  
  if (bootCount == 0){
    Serial.println("Clearing bonded devices");
    SerialBT.deleteAllBondedDevices(); // If reset, allows new device to pair
    n_iter_max = 120; //With 500ms delay, this is 1 minute
  }
  
  int n_iter = 0;
  while (n_iter < n_iter_max || n_iter_max < 0){
    Serial.println("Broadcast #"+String(n_iter));
    send_message(message+","+String(n_iter)+"\n");
    delay(100);

    if (SerialBT.available()){
      Serial.println("Something is alive!");
      break;
    }
    delay(400);
    n_iter++;
  }
  
  bootCount++;
  
  SerialBT.end();
  esp_sleep_enable_timer_wakeup(SLEEP_TIME * uS_TO_S_FACTOR);
  Serial.println("Time awake: "+String(esp_timer_get_time()*1e-6));
  delay(50); //Trying to add a delay to prevent crashes when going to sleep
  esp_deep_sleep_start();
}


void loop() {
  esp_sleep_enable_timer_wakeup(SLEEP_TIME * uS_TO_S_FACTOR);
  esp_deep_sleep_start();
}

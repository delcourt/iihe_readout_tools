from ROOT import TCanvas, TLatex, kBlack, TGraph
from array import array
import json

# Opening JSON file
f = open('ESP32_calib.json')
converter = json.load(f)

min_conv = 999
max_conv = 0
for entry in converter:
    entry  = float(entry)
    if entry < min_conv:
        min_conv = entry
    if entry > max_conv:
        max_conv = entry

def get_charge(volt):
    if volt < min_conv:
        return 0
    if volt > max_conv:
        return 100
    if str(volt) not in converter.keys():
        return 0
    return float(converter[str(volt)])


def print_graph(gg,title, x_axis = None, y_axis = None):
    canvas = TCanvas("c","c",1000,800)
    gg.SetMarkerStyle(20)

    if y_axis == None:
        y_axis = title
    if x_axis == None:
        x_axis = "Time [h]"

    gg.GetYaxis().SetTitle(y_axis)
    gg.GetXaxis().SetTitle(x_axis)
    gg.Draw("AP")
    gg.SetTitle("")
    latex = TLatex()
    latex.SetNDC()
    latex.SetTextAngle(0)
    latex.SetTextColor(kBlack)

    latex.SetTextFont(62)
    latex.SetTextAlign(11)
    latex.SetTextSize(0.04)
    latex.DrawLatex(canvas.GetLeftMargin(),1-canvas.GetTopMargin()+0.01,"CMS")
    latex.SetTextFont(52)
    latex.SetTextSize(0.03)
    latex.DrawLatex(canvas.GetLeftMargin()+0.0664 ,1-canvas.GetTopMargin()+0.01,"Internal")
    latex.SetTextFont(42)
    latex.SetTextAlign(31)
    latex.SetTextSize(0.03)
    latex.DrawLatex(1-canvas.GetRightMargin(),1-canvas.GetTopMargin()+0.01,"IIHE Transport box tests")
    canvas.Print(title.replace(" ","_")+".png")


hum_v  = []
time_v = []
bat_v = []
temp_v = []
n_broadcast_v = []

with open ("humidity.csv", "r") as f:
    for line in f:
        if len(line.replace("\n","").split(",")) != 5:
            print(f"Ignoring {line}")
            continue
        tt,temp,hum,bat,nbrc = line.replace("\n","").split(",")
        time_v.append(float(tt))            # Time
        hum_v.append(float(hum))            # Humidity
        bat_v.append(float(bat))            # Measured battery voltage
        n_broadcast_v.append(float(nbrc))   # Number of broadcasts before Ack
        temp_v.append(float(temp))          # Temperature

t0 = time_v[0]
time_v = [(tt-t0)/3600 for tt in time_v]

gg_1 = TGraph(len(time_v), array("f",time_v), array("f", hum_v))
print_graph(gg_1,"humidity",y_axis="Relative humidity [%]")

gg_2 = TGraph(len(time_v), array("f",time_v), array("f", temp_v))
print_graph(gg_2,"temperature",y_axis="Temperature [C]")

conv = [get_charge(vv) for vv in bat_v]
gg_3 = TGraph(len(time_v), array("f",time_v), array("f", conv))
print_graph(gg_3,"battery",y_axis="battery converted [%]")

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

import sys
import os
import plotter_common
recycle_bin = []
CWD = os.getcwd()


parser = argparse.ArgumentParser(description='Plot module noise')
parser.add_argument('--input' , '-i', metavar='input_dir', type=str, help='Input directory [Required]', required=True)
parser.add_argument('--output', '-o', metavar='output_dir', type=str, help='',  default=f"{CWD}")
parser.add_argument('-b', action='store_true', help='batch process', default=False)
parser.add_argument('--legend' , metavar='leg_pos', type=str, help='Legend position [top|bottom]', choices=['top', 'bottom'], default="top")
parser.add_argument('--y_max' , metavar='val', type=int, help='Max y axis range', default=20)
parser.add_argument('--y_min' , metavar='val', type=int, help='Min y axis range', default=0)
parser.add_argument('--x_max' , metavar='val', type=int, help='Max x axis range', default=1024)
parser.add_argument('--x_min' , metavar='val', type=int, help='Min x axis range', default=0)

parser.add_argument('--hybrids' , metavar='hybrids', type=str, help='Hybrids to plot [0,1,both]', choices=["0","1","both"], default="both")
parser.add_argument('--sensors' , metavar='sensors', type=str, help='Sensors to plot [even,odd,both]', choices=["even","odd","both"], default="both")
parser.add_argument('--merge', action='store_true', help='Merge hybrids on plot', default=False)
parser.add_argument('--hide_cbc', action='store_true', help='Hide CBC lines', default=False)
parser.add_argument('--size_x' , metavar='sizeX', help='Canvas size x',type=int, default=1000)
parser.add_argument('--size_y' , metavar='sizeY', help='Canvas size y',type=int, default=1000)
parser.add_argument('--out_format' , metavar='out_format', type=str, help='Output type [png,pdf,both]', choices=["pdf","png","both"], default="both")

parser.add_argument('--title',  metavar="title", type = str, help = "Custom title")
parser.add_argument('--prefix', metavar="prefix", type = str, help="Only process folders with given prefix" )
parser.add_argument('--print_prefix', action="store_true", help="Show prefix in title" )
parser.add_argument('--hide_voltage', action="store_true", help="Don't show voltage in title")

args = parser.parse_args()

base_dir = args.input
if base_dir[0] != "/":
    base_dir = CWD+"/"+base_dir+"/"
dir_out  = args.output

legend_pos_id = 1
if args.legend == "bottom":
    legend_pos_id = 0
forced_title = args.title

from ROOT import (TFile,TCanvas, TH1F,TF1, gROOT,TLegend, TLine,
                  kBlue , kRed, kCyan)
if args.b:
    gROOT.SetBatch()

legend_pos = [(0.65,0.14,0.96, 0.30),(0.65,0.7,0.96, 0.86)][legend_pos_id]

recycle = []
canvases = []

suffixes = []

def get_multi_noise(files,parity, feh_id = 0):
    hists = []

    for ff in files:
        if  "Results.root" in os.listdir(base_dir+ff):
            meas_file = TFile(base_dir+ff+"/Results.root")
            if parity:
                noise_hist = meas_file.Get(f"Detector/Board_0/OpticalGroup_0/Hybrid_{feh_id}/D_B(0)_O(0)_HybridNoiseOddDistribution_Hybrid({feh_id})")
            else:
                noise_hist = meas_file.Get(f"Detector/Board_0/OpticalGroup_0/Hybrid_{feh_id}/D_B(0)_O(0)_HybridNoiseEvenDistribution_Hybrid({feh_id})")
            noise_hist.SetDirectory(0)
            recycle_bin.append(noise_hist)
            hists.append(noise_hist)

    if len(hists) == 0:
        print("Error, need at least a measurement !")
        return None
    if len(hists) == 1:
        #Remove all error bars :
        for binId in range(hists[0].GetXaxis().GetNbins()):
            hists[0].SetBinError(binId+1,0)
        return hists[0]

    average_hist = hists[0].Clone()
    average_hist.SetDirectory(0)
    for bin_id in range(1,average_hist.GetXaxis().GetNbins()+1):
        avg, rms = 0,0
        for hist in hists:
            avg += hist.GetBinContent(bin_id)
        avg /= len(hists)
        for hist in hists:
            rms += (hist.GetBinContent(bin_id)-avg)**2
        rms /= len(hists)-1
        rms = rms**0.5
        average_hist.SetBinContent(bin_id,avg)
        average_hist.SetBinError(bin_id,rms/(len(hists))**0.5)
    recycle_bin.append(average_hist)
    hists[1].SetDirectory(0)
    recycle_bin.append(hists[1])
    hists[1].SetMarkerStyle(20)
    hists[1].SetMarkerColor(kBlue)
    hists[1].SetMarkerSize(0.1)

    return average_hist


def draw(hh_odd_0, hh_even_0, hh_odd_1, hh_even_1, voltage):
    #Unbiased :
    cc = TCanvas(f"canvas_{len(canvases)}",f"canvas_{len(canvases)}",args.size_x,args.size_y)

    hists   = [hh_odd_0, hh_even_0, hh_odd_1, hh_even_1]
    side    = ["Odd", "Even", "Odd", "Even"]
    hybrid  = [0, 0, 1, 1]
    colors  = [kRed, kBlue, kRed-7, kCyan+1]
    markers = [20,21,22,23]

    leg = TLegend(*legend_pos)
    first_hist = True
    for h_index, hh in enumerate(hists):
        if hh == None:
            continue
        hh.SetMarkerColor(colors[h_index])
        hh.SetLineColor(colors[h_index])
        hh.SetMarkerSize(0.5)
        hh.SetMarkerStyle(markers[h_index])
        hh.GetYaxis().SetRangeUser(args.y_min,args.y_max)
        hh.GetXaxis().SetRange(args.x_min,args.x_max)

        hh.GetYaxis().SetTitle("Noise [VCTH units]")
        hh.GetXaxis().SetTitle("Strip #")
        leg.AddEntry(hh,f"{side[h_index]} strips, hyb {hybrid[h_index]}","lp")
        recycle.append(hh)
        ###GENERATE TITLE
        title = ""
        if args.title:
            title = args.title+" "
        if args.prefix and args.print_prefix:
            title+=args.prefix+" "

        if not args.hide_voltage:
            title+=f"{voltage}V "
        hh.SetTitle(title)
        if first_hist:
            hh.Draw("p")
            first_hist = False
        else:
            hh.Draw("same p")

    lines = []
    if not args.hide_cbc:
        for i in range(7):
            line = TLine((i+1)*127,args.y_min,(i+1)*127,args.y_max)
            line.Draw("same")
            line.SetLineStyle(3)
            lines.append(line)
        leg.AddEntry(lines[-1],"CBC edges","l")
        leg.Draw("same")
    recycle.append([hists,leg,lines])
    return cc


dir_by_voltage = plotter_common.split_by_voltage(base_dir, args)

for voltage in dir_by_voltage:
    print(f"Processing voltage {voltage}")
    h_even_0 = None
    h_odd_0 = None
    h_even_1 = None
    h_odd_1 = None
    if args.hybrids in ["0","both"] and args.sensors in ["even", "both"]:
        h_even_0 = get_multi_noise(dir_by_voltage[voltage],0,0)
    if args.hybrids in ["1","both"] and args.sensors in ["even", "both"]:
        h_even_1 = get_multi_noise(dir_by_voltage[voltage],0,1)
    if args.hybrids in ["0","both"] and args.sensors in ["odd", "both"]:
        h_odd_0 = get_multi_noise(dir_by_voltage[voltage],1,0)
    if args.hybrids in ["1","both"] and args.sensors in ["odd", "both"]:
        h_odd_1 = get_multi_noise(dir_by_voltage[voltage],1,1)

    file_name = f"{args.output}/Noise_"
    if args.prefix:
        file_name+=args.prefix+"_"
    if not args.merge:
        file_name+="hybrid{hyb}_"
    file_name+=f"{voltage}V"

    if not args.merge:
        cc = draw(h_odd_0,h_even_0,None,None,voltage)
        if args.out_format in ["png","both"]:
            cc.Print(file_name.format(hyb=0)+".png")
        if args.out_format in ["pdf","both"]:
            cc.Print(file_name.format(hyb=0)+".pdf")
        cc = draw(None,None,h_odd_1,h_even_1,voltage)
        if args.out_format in ["png","both"]:
            cc.Print(file_name.format(hyb=1)+".png")
        if args.out_format in ["pdf","both"]:
            cc.Print(file_name.format(hyb=1)+".pdf")
    else:
        h_odd_1.SetMarkerColor(kCyan+1)
        h_even_1.SetMarkerColor(kRed-7)
        cc = draw(h_odd_0,h_even_0,h_odd_1,h_even_1,voltage)
        if args.out_format in ["png","both"]:
            cc.Print(file_name+".png")
        if args.out_format in ["pdf","both"]:
            cc.Print(file_name+".pdf")





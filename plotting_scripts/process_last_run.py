#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys,glob,os,datetime

if len(sys.argv) != 2:
    print(f"Error, expecting exactly 1 argument [friendly name], {len(sys.argv)-1} given.")
    sys.exit()
name = sys.argv[1]
CWD = os.getcwd()
for ff in ["Results", "Ph2_ACF"]:
    if not ff in os.listdir(CWD):
        print(f"Error, no folder named '{ff}' in CWD ({CWD})")
        sys.exit()
output_dir = CWD+"/Results"


from ROOT import TFile, TH1
results_list = glob.glob(CWD+'/Ph2_ACF/Results/*')
if len(results_list) == 0:
    print(f"Error, no results in {CWD+'/Ph2_ACF/Results/'}")
input_dir = max(results_list, key=os.path.getctime)

if not "Results.root" in os.listdir(input_dir):
  print("Error, cannot find Hybrid.root in newest folder...")
  sys.exit()

date = datetime.datetime.today().strftime('%Y%m%d')

if not date in os.listdir(output_dir):
  print(f"Creating {CWD}/Results/{date}")
  os.system(f"mkdir {CWD}/Results/{date}")
if name in os.listdir(f"{CWD}/Results/{date}"):
  x = 2
  while name+f"_{x}" in os.listdir(f"{CWD}/Results/{date}"):
    x+=1
  name = name+f"_{x}"

print(f"Moving {input_dir} to {CWD}/Results/{date}/{name}")
os.system(f"mv {input_dir} {CWD}/Results/{date}/{name}")


def get_avg_rms(h):
  avg = 0
  min_val = h.GetBinContent(1)
  max_val = h.GetBinContent(1)
  for bb in range(h.GetNbinsX()):
    x = h.GetBinContent(bb+1)
    if x < min_val:
      min_val = x
    if x > max_val:
      max_val = x
    avg+=x

  avg/=h.GetNbinsX()
  rms = 0
  for bb in range(h.GetNbinsX()):
    rms+=(h.GetBinContent(bb)-avg)**2
  rms/= h.GetNbinsX()
  rms = rms**0.5
  return(avg,rms,min_val,max_val)


ff = TFile(f"Results/{date}/{name}/Results.root")
for hybrid_id in [0,1]:
  print(f"Hybrid #{hybrid_id}")
  dd = ff.Get(f"Detector/Board_0/OpticalGroup_0/Hybrid_{hybrid_id}")
  print(dd)
  avg,rms,mm,MM = get_avg_rms(dd.Get(f"D_B(0)_O(0)_HybridNoiseEvenDistribution_Hybrid({hybrid_id})"))
  print(f"Even strips : Mean noise = {avg:.3f} (RMS = {rms:.3f}) \t [Min; Max] = [{mm:.2f}; {MM:.2f}]")
  avg,rms,mm,MM = get_avg_rms(dd.Get(f"D_B(0)_O(0)_HybridNoiseOddDistribution_Hybrid({hybrid_id})"))
  print(f"Odd strips  : Mean noise = {avg:.3f} (RMS = {rms:.3f}) \t [Min; Max] = [{mm:.2f}; {MM:.2f}]")
  h_ped = dd.Get(f"Chip_0/D_B(0)_O(0)_H({hybrid_id})_PedestalDistribution_Chip(0)")
#  h_strip_ped = dd.Get(f"Chip_0/D_B(0)_O(0)_H({hybrid_id})_StripPedestalDistribution_Chip(0)")
#  print(f"              Mean pedestals = {h_ped.GetMean():.3f} (RMS = {h_ped.GetRMS():.3f}) \t [Min; Max] = [{h_strip_ped.GetMinimum():.2f}; {h_strip_ped.GetMaximum():.2f}]")

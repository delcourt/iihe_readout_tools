#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import plotter_common
import os
from array import array
import argparse

CWD = os.getcwd()

parser = argparse.ArgumentParser(description='Plot module noise trend as a function of voltage')
parser.add_argument('--input' , '-i', metavar='input_dir', type=str, help='Input directory [Required]', required=True)
parser.add_argument('--output', '-o', metavar='output_dir', type=str, help='',  default=f"{CWD}")
parser.add_argument('-b', action='store_true', help='batch process', default=False)
parser.add_argument('--legend' , metavar='leg_pos', type=str, help='Legend position [top|bottom]', choices=['top', 'bottom'], default="top")
parser.add_argument('--y_max' , metavar='val', type=float, help='Max y axis range', default=20)
parser.add_argument('--y_min' , metavar='val', type=float, help='Min y axis range', default=0)
parser.add_argument('--x_max' , metavar='val', type=float, help='Max x axis range', default=805)
parser.add_argument('--x_min' , metavar='val', type=float, help='Min x axis range', default=-5)

parser.add_argument('--hybrids' , metavar='hybrids', type=str, help='Hybrids to plot [0,1,both]', choices=["0","1","both"], default="both")
parser.add_argument('--sensors' , metavar='sensors', type=str, help='Sensors to plot [even,odd,both]', choices=["even","odd","both"], default="both")
parser.add_argument('--out_format' , metavar='out_format', type=str, help='Output type [png,pdf,both]', choices=["pdf","png","both"], default="both")
parser.add_argument('--title',  metavar="title", type = str, help = "Custom title", default="Strip noise average and RMS")
parser.add_argument('--size_x' , metavar='sizeX', help='Canvas size x',type=int, default=1000)
parser.add_argument('--size_y' , metavar='sizeY', help='Canvas size y',type=int, default=1000)
parser.add_argument('--prefix', metavar="prefix", type = str, help="Only process folders with given prefix" )
parser.add_argument('--merge', action='store_true', help='Merge multiple measurements', default=False)
parser.add_argument('--line', action='store_true', help='Draw line between meas', default=False)

parser.add_argument('--depletion', action='store_true', help='Draws n^2 (1/V)', default=False)

args = parser.parse_args()

from ROOT import TFile,TCanvas, TH1F,TF1, gROOT,TLegend, TLine, TGraphErrors, kBlue, kRed, kCyan

if args.b:
    gROOT.SetBatch()

legend_pos_id = 1
if args.legend == "bottom":
    legend_pos_id = 0
legend_pos = [(0.65,0.14,0.96, 0.30),(0.65,0.7,0.96, 0.86)][legend_pos_id]


base_dir = args.input
if base_dir[0] != "/":
    base_dir = CWD+"/"+base_dir+"/"

def get_avg_rms(vec):
    avg = sum(vec) *1./len(vec)
    rms = 0
    for x in vec:
        rms += (x-avg)**2
    rms/= len(vec)
    rms = rms**0.5
    return avg,0

dir_by_voltage = plotter_common.split_by_voltage(base_dir, args)
voltages = sorted(list(dir_by_voltage.keys()))

data = {}
for vv in voltages:
    for ff_name in dir_by_voltage[vv]:
        ff = TFile(base_dir+ff_name+"/Hybrid.root")
        for hybId in range(2):
            dd = ff.Get(f"Detector/Board_0/OpticalGroup_0/Hybrid_{hybId}")
            hist_data  = [
                plotter_common.get_hist_data(dd.Get(f"D_B(0)_O(0)_HybridNoiseEvenDistribution_Hybrid({hybId})")),
                plotter_common.get_hist_data(dd.Get(f"D_B(0)_O(0)_HybridNoiseOddDistribution_Hybrid({hybId})")),
            ]

            plotter_common.data_filler(data,hist_data[0],vv,hybId,"even")
            plotter_common.data_filler(data,hist_data[1],vv,hybId,"odd")


#Data : [Voltage][Hybrid]["even"/"odd"]


#Getting summary :
data_summary = {}
for vv in data:
    for hh in data[vv]:
        for ss in data[vv][hh]:
            if args.merge:
                dd = []
                for xx in data[vv][hh][ss]:
                    dd+=xx
                plotter_common.data_filler(data_summary,get_avg_rms(dd),vv,hh,ss)
            else:
                for dd in data[vv][hh][ss]:
                    plotter_common.data_filler(data_summary,get_avg_rms(dd),vv,hh,ss)

# Generating graphs:
# Key list :
kk_list = [(0,1),("even","odd")]
gg_data = []
for hh in kk_list[0]:
    for ss in kk_list[1]:
        gg_info = {}
        gg_info ["debug"] = f"{hh} / {ss}"
        gg_info ["data"] = []
        for vv in data_summary:
            failed = True
            if hh in data_summary[vv]:
                if ss in data_summary[vv][hh]:
                    failed = False
                    for entry in data_summary[vv][hh][ss]:
                        gg_info["data"].append((vv,entry))
            if failed:
                print(f"ERROR, can't get volt = {vv}, h = {hh}, s = {ss}")
        gg_data.append(gg_info)


cc = TCanvas("Canvas","Canvas",args.size_x,args.size_y)

leg = TLegend(*legend_pos)

graphs = []

cols = [kBlue, kRed, kBlue, kRed]
markers = [20,20,21,21]
line_style = [1,1,1,1]
#gg_data  : hyb 0, even; hyb0, odd; hyb1, even; hyb1, odd
titles = [ "Hybrid 0, even strips",
           "Hybrid 0, odd strips",
           "Hybrid 1, even strips",
           "Hybrid 1, odd strips",
         ]


first = True
for graph_id in range(4):
    hybId  = graph_id /2
    sensId = graph_id %2
    print(gg_data[graph_id]["debug"])

    if args.hybrids in ["0","1"] and int(args.hybrids) != int(hybId):
        print("Skipping")
        continue
    if args.sensors in ["even","odd"]:
        if (args.sensors == "even" and sensId == 1) or (args.sensors == "odd" and sensId == 0):
            print("Skipping")
            continue
    dd = gg_data[graph_id]["data"]
    tt = titles[graph_id]
    gg = None
    if args.depletion == False:
        gg = TGraphErrors(len(dd), array('f',[xx[0]    for xx in dd]),
                                array('f',[xx[1][0] for xx in dd]),
                                array('f',[0        for xx in dd]),
                                array('f',[xx[1][1] for xx in dd])
                        )
    else:
        gg = TGraphErrors(len(dd), array('f',[xx[0]**0.5 for xx in dd]),
                                array('f',[1./(xx[1][0]-2) if xx[1][0] > 2 else 0 for xx in dd]),
                                array('f',[0 for xx in dd]),
                                array('f',[xx[1][1]*1./(xx[1][0]-2)**2 if xx[1][0] > 2 else 0 for xx in dd])
                        )
    gg.SetMarkerColor(cols[graph_id])
    gg.SetLineColor(cols[graph_id])
    gg.SetLineStyle(line_style[graph_id])
    gg.SetMarkerStyle(markers[graph_id])
    if first:
        gg.SetTitle(args.title)
        if args.depletion == False:
            gg.GetXaxis().SetTitle("Measurement number")
            gg.GetYaxis().SetTitle("Noise [VCTH]")
        else:
            gg.GetXaxis().SetTitle("sqrt(Bias voltage) [Volts^0.5]")
            gg.GetYaxis().SetTitle("1/(Noise-2) [VCTH^-1]")
        if args.line:
            gg.Draw("A PL")
        else:
            gg.Draw("A P")
        gg.GetXaxis().SetRangeUser(args.x_min,args.x_max)
        gg.GetYaxis().SetRangeUser(args.y_min,args.y_max)
        first = False
    else:
        if args.line:
            gg.Draw("same PL")
        else:
            gg.Draw("same P")
    graphs.append(gg)
    leg.AddEntry(gg,tt,"lp")


leg.Draw("same")
if args.out_format in ["png","both"]:
    cc.Print(args.output+"/Noise_trend.png")
if args.out_format in ["pdf","both"]:
    cc.Print(args.output+"/Noise_trend.pdf")

cc.Print(args.output+"/Noise_trend.root")

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import datetime
from PyQt5.QtCore import Qt, QThread, QObject,pyqtSignal, QMutex, QTimer
from PyQt5.QtGui import QIntValidator, QIcon, QPixmap,QMovie
from PyQt5.QtWidgets import (QCheckBox, QWidget, QApplication, QVBoxLayout, QPushButton, QFileDialog, QMessageBox, QGridLayout,
                            QHBoxLayout, QLabel, QGroupBox,QComboBox, QLineEdit, QScrollArea, QProgressDialog, QProgressBar,QTextEdit)

import os.path, yaml, csv,os,sys,json, argparse
from glob import glob
import subprocess

webhook = "https://mattermost.web.cern.ch/hooks/5duufkqidtdxbjswefb9xp8fzh"
parser = argparse.ArgumentParser(description='Plot module noise')
parser.add_argument('--input' , '-i', metavar='input_dir', type=str, help='Default input directory',default = "None")
parser.add_argument('--debug' , '-d',help='Debug mode. Will not upload to Gabriel, but will post to mattermost.', default=False,  action='store_true')
parser.add_argument('--no-mattermost' , help='Will not post to mattermost.', default=False,  action='store_true')

args = parser.parse_args()
print(args)
step_translation = {
            "mod_bonded" : "Bonded",
            "mod_final" : "Final"
}

naming_convention = {"Even":"Bottom", "Odd":"Top"}

skip_upload     = args.debug
skip_mattermost = args.no_mattermost

def timestamp_to_str(timestamp):
    '''
    Convert local timestamps to GABRIEL time strings.
    GABRIEL time is ISO 8601 format (UTC+00:00 -> trailing Z).
    '''
    dt_utc = datetime.utcfromtimestamp(timestamp)
    #import pytz
    #tz_bru = pytz.timezone('Europe/Brussels')
    #dt_bru = tz_bru.localize(dt_utc)

    # Convert to ISO 8601 format
    time_str = dt_utc.isoformat()

    # Add trailing Z (for UTC+00:00)
    # datetime does not add this, 
    # as by default it is time zone blind
    time_str += 'Z'

    return time_str

def get_avg_rms(h):
  avg = 0
  min_val = h[0]
  max_val = h[0]
  for x in h:
    if x < min_val:
      min_val = x
    if x > max_val:
      max_val = x
    avg+=x

  avg/=len(h)
  rms = 0
  for x in h:
     rms+=(x-avg)**2
  rms/= len(h)
  rms = rms**0.5
  return(avg,rms,min_val,max_val)

def hist_to_list(h):
    return [h.GetBinContent(bb+1) for bb in range(h.GetNbinsX())]


window_width  = 600
global_frac   = 0.94
lcol_frac     = 0.3

import time,json
auto_widget_dic = {}

def button_creator(name, action, set_width = -1, unique_name = None):
    button = QPushButton(name)
    button.clicked.connect(action)
    if set_width > 0:
        button.setFixedWidth( int(set_width) )
    if unique_name:
        auto_widget_dic[unique_name] = button
    return button

def label_creator(name,set_width = -1, unique_name = None):
    label = QLabel(name)
    if set_width > 0:
        label.setFixedWidth( int(set_width) )
    if unique_name:
        auto_widget_dic[unique_name] = label
    return label

def checkbox_creator(name,action = None, unique_name = None):
    cb = QCheckBox()
    cb.setChecked(1)
    cb.setObjectName(name)

    if action != None:
        cb.clicked.connect(action)

    if unique_name:
        auto_widget_dic[unique_name] = cb
    return cb

def valbox_creator(default = None, set_width = -1, val_min = None, val_max = None, unique_name = None):
    if default:
        vb = QLineEdit(f"{default}")
    else:
        vb = QLineEdit("")

    if set_width > 0:
        vb.setFixedWidth( int(set_width) )

    if val_min != None and val_max != None:
        int_validator = QIntValidator()
        int_validator.setTop(val_max)
        int_validator.setBottom(val_min)
        vb.setValidator(int_validator)

    if unique_name:
        auto_widget_dic[unique_name] = vb

    return vb

def input_creator(default = None, set_width = -1, unique_name = None):
    if default:
        vb = QLineEdit(f"{default}")
    else:
        vb = QLineEdit("")

    if set_width > 0:
        vb.setFixedWidth( int(set_width) )
    if unique_name:
        auto_widget_dic[unique_name] = vb

    return vb

def question_popup(text):
   msgBox = QMessageBox()
   msgBox.setIcon(QMessageBox.Information)
   msgBox.setText(text)
   msgBox.setWindowTitle("Alert")
   msgBox.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
   #msgBox.buttonClicked.connect(msgButtonClick)

   returnValue = msgBox.exec()
   if returnValue == QMessageBox.Ok:
       return 1
   else:
       return 0

def input_field_creator(label_text, input_default = None, input_unique_name = None):
    the_group  = QGroupBox()
    the_layout = QHBoxLayout(the_group)
    the_layout.addWidget(label_creator(label_text,window_width*global_frac*lcol_frac),0)
    the_layout.addWidget(input_creator(input_default, window_width*global_frac*(1-lcol_frac),input_unique_name),1)

    return the_group

class gui(QObject):

    def __init__(self):        
        super(gui, self).__init__()
        self.app = QApplication([])
        self.dir_loaded = None

        window = QWidget()
        self.window = window
        window.setGeometry(0, 0, window_width, 800)
        window.setWindowTitle("GABRIEL readout uploader")

        main_box_group      = QGroupBox(window)
        main_box_widgets    = QVBoxLayout(main_box_group)
        # title = QLabel("GABRIEL readout uploader")
        # title.setFixedHeight(50)
        # title.setFixedWidth(int(global_frac*window_width))
        # title.setAlignment(Qt.AlignCenter)

        
        # main_box_widgets.addWidget(title,0)
        main_box_widgets.addWidget(label_creator("No directory loaded",unique_name="dir_loaded"),2)
        main_box_widgets.addWidget(button_creator("Load results directory",self.download_all_popup),1)

        test_loaded_widgets    = QHBoxLayout()
        main_box_widgets.addLayout(test_loaded_widgets)


        test_loaded_widgets.addWidget(label_creator("IV curve: ",set_width=50))
        test_loaded_widgets.addWidget(label_creator("",unique_name="iv_curve_check",set_width=30))
        test_loaded_widgets.addWidget(label_creator("Readout: ",set_width=50))
        test_loaded_widgets.addWidget(label_creator("",unique_name="readout_check",set_width=30))

        self.pixmap_yes = QPixmap(os.environ["IIHE_TOOLS_BASE_DIR"]+'/common/assets/Green_tick_small.png')
        self.pixmap_no  = QPixmap(os.environ["IIHE_TOOLS_BASE_DIR"]+'/common/assets/Red_X_small.png')

        auto_widget_dic["iv_curve_check"].setPixmap(self.pixmap_no)
        auto_widget_dic["readout_check"].setPixmap(self.pixmap_no)

        main_box_widgets.addWidget(input_field_creator("Operator","Unknown","operator_field"))
        main_box_widgets.addWidget(input_field_creator("Part id" ,"Unknown","part_field"))
        main_box_widgets.addWidget(input_field_creator("Bias" ,"Unknown","bias_field"))
        ##### Skeleton test section
        self.skeleton_checkbox = QCheckBox("Skeleton test")
        main_box_widgets.addWidget(self.skeleton_checkbox)
        main_box_widgets.addWidget(input_field_creator("FEH 1","None","Hybrid_1"))
        main_box_widgets.addWidget(input_field_creator("FEH 2","None","Hybrid_2"))
        self.skeleton_checkbox.stateChanged.connect(self.update_skeleton_state)
        self.update_skeleton_state(0)

        ##### VTRX+ gluing section
        self.vtrx_checkbox = QCheckBox("Attach VTRX+")
        main_box_widgets.addWidget(self.vtrx_checkbox)
        main_box_widgets.addWidget(input_field_creator("VTRX+ Id","None","vtrx_id"))
        main_box_widgets.addWidget(button_creator("Attach_vtrx",self.upload_attach_vtrx, set_width=100,unique_name="attach_vtrx_button"))

        self.vtrx_checkbox.stateChanged.connect(self.update_vtrx_state)




        main_box_widgets.addWidget(label_creator("User comment:"))
        self.user_comment = QTextEdit()
        self.user_comment.setPlaceholderText("Add any comment on the measurement here...")
        main_box_widgets.addWidget(self.user_comment)
        main_box_widgets.addWidget(button_creator("Upload everything",self.upload, unique_name="upload_button"))
        self.test_label = QLabel()
        main_box_widgets.addWidget(self.test_label)
        self.has_module_test = False
        self.has_IV = False
        self.update_vtrx_state(0)

        if args.input != "None":
            self.dir_loaded = args.input
            self.load_dir()

        if skip_upload:
            message = "Application launched in debug mode. Nothing will be uploaded"
            if skip_mattermost:
                message+= " and nothing will be posted to mattermost."
            else:
                message+= " but a message will be posted to mattermost."
            self.alert(message)
        else:
            if skip_mattermost:
                self.alert("Application launched in 'no-mattermost' mode. Nothing will be posted to mattermost.")
        window.show()
        self.app.exec_()
        



    def update_skeleton_state(self,state):
        auto_widget_dic["Hybrid_1"].setDisabled(state!=2)
        auto_widget_dic["Hybrid_2"].setDisabled(state!=2)
    
    def update_vtrx_state(self,state):
        auto_widget_dic["vtrx_id"].setDisabled(state!=2)
        auto_widget_dic["attach_vtrx_button"].setDisabled(state!=2)
        if state == 2:
            auto_widget_dic["upload_button"].setText("Upload everything, attach VTRX and quit")
        else:
            auto_widget_dic["upload_button"].setText("Upload everything and quit")



    def download_all_popup(self):
        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.Directory)

        if dlg.exec_():
            filenames = dlg.selectedFiles()
            if len(filenames) < 1:
                self.alert("Error, no file specified")
                return
            binFileName = filenames[0]
            self.dir_loaded = binFileName
            self.load_dir()

    def upload_attach_vtrx(self):
        if self.attach_vtrx():
            self.vtrx_checkbox.setCheckState(False)
            self.update_vtrx_state(0)

    def test_parser(self,ret):
        rc,em,rm = ret
        for entry in em:
            self.alert_message+=entry+"\n"
        for entry in rm:
            self.report_message+=entry+"\n"
        return rc



    def upload(self):
        auto_widget_dic["upload_button"].setDisabled(True)
        auto_widget_dic["upload_button"].repaint()
        self.part_key = 0
        self.report_message = ""
        self.alert_message  = ""
        if self.has_module_test:            
            if not self.test_parser(self.generate_test_info()):
                self.alert(self.alert_message)
                self.alert(self.report_message)
                auto_widget_dic["upload_button"].setDisabled(False)
                return 0
            if not self.test_parser(self.upload_readout()):
                self.alert(self.alert_message)
                self.alert(self.report_message)
                auto_widget_dic["upload_button"].setDisabled(False)
                return 0
            
        
        if self.has_IV:
            if not self.test_parser(self.generate_iv_info()):
                self.alert(self.alert_message)
                self.alert(self.report_message)
                auto_widget_dic["upload_button"].setDisabled(False)
                return 0
            if not self.test_parser(self.upload_IV()):
                self.alert(self.alert_message)
                self.alert(self.report_message)
                auto_widget_dic["upload_button"].setDisabled(False)
                return 0


        if self.vtrx_checkbox.isChecked():
            if not self.attach_vtrx():
                self.alert(self.alert_message)
                auto_widget_dic["upload_button"].setDisabled(False)
                return 0
        if self.part_key > 0:
            self.report_message = f":point_right: Functional test for part [{auto_widget_dic['part_field'].text()}](https://gabriel.iihe.ac.be/parts/{self.part_key})\n"+self.report_message
        else:
            self.report_message = f":point_right: Functional test for part {auto_widget_dic['part_field'].text()}\n"+self.report_message
        self.alert(self.alert_message)
        if skip_mattermost:
            self.alert("Mattermost message would have been:\n"+self.report_message)
        else:
            if skip_upload:
                self.report_message = "This is a test, debug mode was enabled in the uploader. Nothing was uploaded to Gabriel.\n"+self.report_message
            import requests
            myobj = {'text': self.report_message }
            x = requests.post(webhook, json = myobj)
            print(x)

        self.window.close()

    def upload_IV(self):
        alert_messages  = []
        report_messages = []
        test_number = 0
        try:
            # # Append folder below to path to import client
            file_dir = os.path.dirname(os.path.abspath(__file__))
            sys.path.append(os.path.join(file_dir, '..'))
            sys.path.append(os.path.join(file_dir, '../gabrielClient'))
            from GabrielClient import GabrielClient
            client = GabrielClient(verbose=2)
            client.login()
            #First, check if module is registered...
            try:
                self.part_key = client.find_part_detailed(self.iv_data["part_key"])["id"]
            except Exception as e:
                print(e)
                alert_messages.append("Unable to find part in DB! Check name.")
                report_messages.append(":x: Unable to upload IV to Gabriel (part non-existent)!")
                return 0,alert_messages,report_messages
            try:
                if skip_upload:
                    test_number = 0
                    alert_messages.append("DEBUG, not really uploading")
                else:
                    test_number = client.create_test(test_data=self.iv_data, sub_type_data=self.iv_sub_data)
                print(test_number)
            except Exception as e:
                alert_messages.append(f"Unable to create test! {e}")
                report_messages.append(f":x: Unable to upload IV to Gabriel (test not created: {e})!")
                return 0,alert_messages,report_messages
            finally:
                client.close()
        except Exception as e:
            alert_messages.append(f"Unable to upload IV to GABRIEL! {e}")
            report_messages.append(f"Unable to upload IV to GABRIEL! {e}")
            return 0,alert_messages, report_messages
        report_messages.append(f":white_check_mark: IV uploaded to gabriel! [Link](https://gabriel.iihe.ac.be/test_iv_logs/{test_number})")
        alert_messages.append("Successful IV upload!")
        return 1,alert_messages, report_messages

    def upload_readout(self):
        alert_messages  = []
        report_messages = []

        test_number = 0
        try:
            # # Append folder below to path to import client
            file_dir = os.path.dirname(os.path.abspath(__file__))
            sys.path.append(os.path.join(file_dir, '..'))
            sys.path.append(os.path.join(file_dir, '../gabrielClient'))
            from GabrielClient import GabrielClient
            client = GabrielClient(verbose=2)
            client.login()
            if self.part_type == "module":
                #First, check if module is registered...
                try:
                    self.part_key = client.find_part_detailed(self.header_data["part_key"])["id"]
                except Exception as e:
                    alert_messages.append(f"Unable to find part {self.header_data['part_key']} in DB! Check name.")
                    report_messages.append(f":x: Unable to find part {self.part_key} in DB! Check name.")
                    return 0, alert_messages, report_messages
                try:
                    data = self.header_data
                    sub_data = self.data_dic
                    os.system("touch /tmp/test.txt")
                    files = [
                        ('csv_file', open(self.dir_loaded+'/gabriel_summary.csv','rb')),
                        ('xml_file', open('/tmp/test.txt','rb')),
                        ('upload_files[]', open(f'/tmp/{self.dir_loaded.split("/")[-1]}.tar.gz','rb'))
                    ]
                    if skip_upload:
                        test_number = 0
                        alert_messages.append("DEBUG, not really uploading")
                    else:
                        test_number = client.create_test(test_data=data, sub_type_data=sub_data, files_=files)
                finally:
                    client.close()
            elif self.part_type == "skeleton":
                print("Uploading skeleton...")
                #First, check if all parts are registered...
                for part_type in ["left_feh_key","right_feh_key","seh_key"]: #keep seh_key as last, will be used for redirect
                    try:
                        self.part_key = client.find_part_detailed(self.data_dic[part_type])["id"]
                    except Exception as e:
                        print(e)
                        answer = question_popup(f"Unable to find {part_type} ({self.data_dic[part_type]}) in DB! Do you want to create it?")
                        if answer == 0:
                            return 0,alert_messages,report_messages
                        else:
                            self.alert("Not implemented!")
                            return 0,alert_messages,report_messages
                try:
                    data = dict(self.data_dic,**self.header_data)
                    # for d in self.data_dic:
                    #     data.update(d)
                    os.system("touch /tmp/test.txt")
                    files = [
                        ('csv_file', open(self.dir_loaded+'/gabriel_summary.csv','rb')),
                        ('xml_file', open('/tmp/test.txt','rb')),
                        ('upload_files[]', open(f'/tmp/{self.dir_loaded.split("/")[-1]}.tar.gz','rb'))
                    ]
                    if skip_upload:
                        test_number = 0
                        alert_messages.append("DEBUG, not really uploading")
                    else:
                        test_number = client.create_skeleton_test(test_data=data, files_=files)
                    print(test_number)
                    # gabriel_answer = client.create_test(test_data=data, sub_type_data=sub_data, files_=files)
                finally:
                    client.close()
                print("Done uploading!")
        except Exception as e:
            
            alert_messages.append("Error during upload to DB! {e}")
            report_messages.append(":x: Unable to upload RO test to DB")
            return 0,alert_messages, report_messages
        
        
        # if test_number != 0:
        #         subprocess.Popen(['xdg-open',f"https://gabriel.iihe.ac.be/test_module_logs/{test_number}"])
        #     else:
        #         subprocess.Popen(['xdg-open',f"https://gabriel.iihe.ac.be/test_hybrid_logs/{test_number}"])
        if self.part_type == "module":
            report_messages.append(f":white_check_mark: Read-out uploaded to gabriel! [link](https://gabriel.iihe.ac.be/test_module_logs/{test_number})")
        else:
            report_messages.append(f":white_check_mark: Read-out uploaded to gabriel! [link](https://gabriel.iihe.ac.be/test_hybrid_logs/{test_number})")
        alert_messages.append("Successful Read-out upload!")
        return 1,alert_messages,report_messages
        
    def attach_vtrx(self):
        alert_message = []
        report_messages = []
        try:
            # # Append folder below to path to import client
            file_dir = os.path.dirname(os.path.abspath(__file__))
            sys.path.append(os.path.join(file_dir, '..'))
            sys.path.append(os.path.join(file_dir, '../gabrielClient'))
            from GabrielClient import GabrielClient

            client = GabrielClient(verbose=2)
            client.login()
            vtrx_id = auto_widget_dic["vtrx_id"].text().strip()
            if len(vtrx_id) == len("16611014922")+2 and vtrx_id.startswith("00"):
                vtrx_id = vtrx_id[2:]
            try:
                data = {
                    'part_key': auto_widget_dic["part_field"].text(),
                    'assembled_at': client.timestamp_to_str(time.time()),
                    'assembly_type_id': 11,
                }
                sub_data = {
                    'vtrx_key': vtrx_id,
                    'comments': 'Added using read-out uploader'
                }
                if skip_upload:
                    alert_message.append("DEBUG, not really uploading")
                else:
                    client.create_assembly(assembly_data=data, sub_type_data=sub_data)
            finally:
                client.close()
        except Exception as e:
            alert_message.append(f"Error while attaching VTRX+: {e}")
            report_messages.append(f":x: Unable to attach vtrx+")
            return 0,alert_message,report_messages
        alert_message.append("VTRX+ attached in GABRIEL!")
        report_messages.append(":white_check_mark: TRX+ attached to module")
        return 1,alert_message,report_messages

    def load_dir(self):
        if self.dir_loaded == None:
            return
        
        self.has_module_test = "Results.root" in os.listdir(self.dir_loaded)
        
        self.IV_files = glob(self.dir_loaded+"/IV_*.yml")
        n_files = len(self.IV_files)
        for ii in range(len(self.IV_files)):
            if "IV_test_" in self.IV_files[n_files-ii-1]:
                self.IV_files.pop(ii)
        self.has_IV          = len(self.IV_files) == 1
        if self.has_module_test:
            auto_widget_dic["readout_check"].setPixmap(self.pixmap_yes)
        else:
            auto_widget_dic["readout_check"].setPixmap(self.pixmap_no)
        if self.has_IV:
            auto_widget_dic["iv_curve_check"].setPixmap(self.pixmap_yes)
        else:
            auto_widget_dic["iv_curve_check"].setPixmap(self.pixmap_no)


        ### Bias is only well defined for readout data:
        if self.has_module_test:
        #define bias as majority voltage that is > 5V        
            if "3IIHEHV.yml" in os.listdir(self.dir_loaded):
                bias_dic = {}
                with open(self.dir_loaded+"/3IIHEHV.yml") as stream:
                    try:
                        for meas in yaml.safe_load(stream):
                            this_bias = int(float(meas["Channels"]["HV0"]["Voltage"])+0.5)
                            if not this_bias in bias_dic.keys():
                                bias_dic[this_bias] = 0
                            bias_dic[this_bias] += 1
                            
                        bias_list = [(bias_dic[bb],bb) for bb in bias_dic]
                        bias_list.sort()
                        bias = 0
                        for entry in reversed(bias_list):
                            if entry[1] < 20:
                                continue
                            if entry[0] < 5:
                                break 
                            bias = entry[1]
                            break
                                
                        auto_widget_dic["bias_field"].setText(str(int(bias)))
                    except yaml.YAMLError as exc:
                        print(exc)
        
        fList = glob(self.dir_loaded+"/summary_AnalyseMonitorData_*.yml")
        part_id = "None"
        if len(fList) == 1:
            with open(fList[0]) as stream:
                try:
                    full_yml = yaml.safe_load(stream)
                    auto_widget_dic["operator_field"].setText(full_yml["Info"]["Operator"])
                    part_id = full_yml["Info"]["Module_ID"]
                    auto_widget_dic["part_field"].setText(part_id)

                except yaml.YAMLError as exc:
                    print(exc)                

        auto_widget_dic["dir_loaded"].setText(self.dir_loaded)
        if part_id[:3] == "2S_":
            print("Detected that we have a module.")
        elif part_id[:5] == "2SSEH":
            print("Detected that we have a skeleton!")
            self.skeleton_checkbox.setChecked(True)
        else:
            self.alert(f"Warning, the part id : {part_id} doesn't look like a module or a skeleton")

    def alert(self,txt):
        msgBox=QMessageBox()
        msgBox.setText(txt)
        msgBox.exec()

    def hello_world(self):
        self.alert("Hello")

    def generate_test_info(self):
        alert_messages = []
        report_messages = []

        bias = 0
        try:
            bias = float(auto_widget_dic["bias_field"].text())
        except Exception as e:
            print(e)
            alert_messages.append(f"Unable to convert bias {auto_widget_dic['bias_field'].text()} to float...")
            return 0,alert_messages,report_messages
        
        ###What kind of test do we have?
        part_type = "module"
        if self.skeleton_checkbox.checkState() == 2:
            part_type = "skeleton"
        self.part_type = part_type
        part_key = auto_widget_dic["part_field"].text()
        if part_type == "module" and part_key[:5] == "2SSEH":
            alert_messages.append("Error, trying to upload a skeleton as a module. Please check inputs.")
            return 0, alert_messages, report_messages
        if part_type == "skeleton" and part_key[:3] == "2S_":
            alert_messages.append("Error, trying to upload a module as a skeleton. Please check inputs.")
            return 0, alert_messages, report_messages
        
        data_dic = {}
        data_dic['bias_voltage'] = bias
        open_thresh = 1
        noisy_thresh = 4
        nnoisy_thresh = 0
        nopen_thresh = 0
        if bias >= 100:
            nopen_thresh = 4
            noisy_thresh = 10
            nnoisy_thresh = 5
        if bias >= 350:
            open_thresh = 4
            noisy_thresh = 9
            nnoisy_thresh = 5
            nopen_thresh  = 5

        all_open = 0
        all_noisy = 0
        raw_data = {}
        try:
            from ROOT import TFile
            ff = TFile(self.dir_loaded+"/Results.root")
            for hybrid_id in [0,1]:
                dd = ff.Get(f"Detector/Board_0/OpticalGroup_0/Hybrid_{hybrid_id}")
                for side in ["Even","Odd"]:  
                    data = hist_to_list(dd.Get(f"D_B(0)_O(0)_HybridNoise{naming_convention[side]}Distribution_Hybrid({hybrid_id})"))
                    avg,rms,mm,MM = get_avg_rms(data)
                    side = side.lower()
                    hybrid_name = "right"
                    if hybrid_id == 1:
                        hybrid_name = "left"
                    data_dic[f"{hybrid_name}_feh_{side}_avg"] = avg
                    data_dic[f"{hybrid_name}_feh_{side}_rms"] = rms
                    data_dic[f"{hybrid_name}_feh_{side}_max"] = MM
                    data_dic[f"{hybrid_name}_feh_{side}_min"] = mm
                    n_open  = sum([ x < open_thresh for x in data])
                    n_noisy = sum([ x > noisy_thresh for x in data])
                    data_dic[f"{hybrid_name}_feh_{side}_nopen"] = n_open
                    data_dic[f"{hybrid_name}_feh_{side}_nnoisy"] = n_noisy 
                    all_open += n_open
                    all_noisy += n_noisy
                    raw_data[f"{hybrid_name}_feh_{side}"]     = data
        except Exception as e:
            alert_messages.append(f"Unable to parse root file! {e}")
            print(e)
            return 0, alert_messages, report_messages

        try:
            with open(self.dir_loaded+'/gabriel_summary.csv', 'w', newline='') as csvfile:
                filewriter = csv.writer(csvfile, delimiter=',',
                                        quotechar='|', quoting=csv.QUOTE_MINIMAL)
                print(data_dic)
                for key in data_dic:
                    filewriter.writerow([key,data_dic[key]])   
                filewriter.writerow([])   

                tested_items = [k for k in raw_data.keys()]
                n_entries = len(raw_data[tested_items[0]])
                filewriter.writerow(["Strip Id"]+[key for key in tested_items])

                for entry_id in range(n_entries):
                    filewriter.writerow([entry_id]+[raw_data[key][entry_id] for key in tested_items])
        except Exception as e:
            print(e)
            alert_messages.append(f"Unable to generate results csv! {e}")
            return 0, alert_messages, report_messages



        data_dic['measurement'] = json.dumps({
                    'Strip Id' : [i for i in range(len(raw_data["right_feh_even"]))],
                    'hyb0_Even' : raw_data["right_feh_even"],
                    'hyb0_Odd' : raw_data["right_feh_odd"],
                    'hyb1_Even' : raw_data["left_feh_even"],
                    'hyb1_Odd' : raw_data["left_feh_odd"],
                })

        data_dic['comments']            =  'Automatic upload'



        data_dic["testTime"] = os.path.getmtime(f"{self.dir_loaded}/Results.root")
        summary_list = glob(args.input+"/summary_AnalyseMonitorData_*.yml")
        if not len(summary_list) == 1:
            self.alert(f"Error, number of summary files = {len(summary_list)}")
            return 0
        step = ""
        with open(summary_list[0]) as stream:
                try:
                    full_yml = yaml.safe_load(stream)
                    step = step_translation[full_yml["Info"]["Run_type"]]
                except yaml.YAMLError as exc:
                    print(exc)                
        data_dic['construction_step'] = step,
        header_data = {'test_station_key' : 'RO1'}
        header_data['in_spec'] = int(all_noisy <= nnoisy_thresh and all_open <= nopen_thresh)
        print("IN SPEC: ",header_data['in_spec'])
        print(int(all_noisy <= nnoisy_thresh and all_open <= nopen_thresh))
        print(all_noisy <= nnoisy_thresh)
        print(all_open <= nopen_thresh)
        print("END OF SPEC CHECK")

        if all_noisy > nnoisy_thresh:
            report_messages.append(f":warning: {all_noisy} channels (> {nnoisy_thresh})")
        if all_open  > nopen_thresh:
            report_messages.append(f":warning: {all_open} channels (> {nopen_thresh})")


        header_data["measured_at"] = timestamp_to_str(data_dic["testTime"])
        print(header_data["measured_at"])
        data_dic["operator"]       = auto_widget_dic["operator_field"].text()


        if part_type == "skeleton":
            ### Getting parts id...
            data_dic['seh_key'] = auto_widget_dic["part_field"].text()
            data_dic['left_feh_key'] = None
            data_dic['right_feh_key'] = None
            
            for hyb_key in ["Hybrid_1","Hybrid_2"]:
                hyb = auto_widget_dic[hyb_key].text()
                if "L" in hyb:
                    data_dic['left_feh_key'] = hyb
                elif "R" in hyb:
                    data_dic['right_feh_key'] = hyb
                else:
                    self.alert(f"Can't assign {hyb} to left or right hybrid")
                    return 0, alert_messages, report_messages
            if data_dic['left_feh_key'] == None:
                alert_messages.append(f"Can't assign left hybrid!")
                return 0, alert_messages, report_messages
            if data_dic['right_feh_key'] == None:
                alert_messages.append(f"Can't assign right hybrid!")
                return 0, alert_messages, report_messages
            
        else:
            header_data['test_type_id'] = 5
            header_data["part_key"] = auto_widget_dic["part_field"].text()

        data_dic['comments']            =  self.user_comment.toPlainText()

        self.data_dic = data_dic
        self.header_data = header_data

        os.system(f"tar -czvf /tmp/{self.dir_loaded.split('/')[-1]}.tar.gz {self.dir_loaded}")
        if header_data['in_spec']:
            report_messages.append(":white_check_mark: GOOD read-out test")
        else:
            report_messages.append(":x: BAD read-out test (@delcourt, please investigate)")
            alert_messages.append("WARNING! Read-out test seems to be bad!")
        return 1, alert_messages, report_messages



    def generate_iv_info(self):
        alert_message = []
        report_message = []
        try:                    
            iv_file = self.IV_files[0] #It was checked earlier that this makes sense

            bias_v    = []
            current_v = []
            temp1_v = []
            humi_v = []
            temp2_v = []
            lumi_v = []

            with open(iv_file) as stream:
                try:
                    full_yml = yaml.safe_load(stream)
                    for meas in full_yml["Data"]:
                        try:
                            bias    = float(meas["Voltage"])
                            current = float(meas["Current"])
                            temp    = float(meas["Temperature"])
                            humi    = float(meas["Relative Humidity"])

                            bias_v.append(-1*bias)
                            current_v.append(current)
                            temp1_v.append(temp)
                            humi_v.append(humi)
                            temp2_v.append(None)
                            lumi_v.append(None)
                        except Exception as exc:
                            print(exc)
                    part_id = full_yml["Info"]["ID"]
                except Exception as exc:
                    print(exc)

            summary_list = glob(args.input+"/summary_AnalyseMonitorData_*.yml")
            if not len(summary_list) == 1:
                alert_message.append(f"Error, number of summary files = {len(summary_list)}")
                return 0, alert_message, report_message

            step = ""
            with open(summary_list[0]) as stream:
                    try:
                        full_yml = yaml.safe_load(stream)
                        step = step_translation[full_yml["Info"]["Run_type"]]
                    except yaml.YAMLError as exc:
                        print(exc)                

            test_time = os.path.getmtime(summary_list[0])

            try:

                #### Setting the 'in_spec' flag
                #### We need to have at least some current (ie, 200nA @ 650V)
                #### Not more than 7.25µA @ 650V
                #### I800/I600 < 2.5
                #### If we only go to 350V, what should be the limit?
                #### ----> Setting it very tight for the time being, at 1.0 µA to make sure light was off...

                #### Combining IV in single list and sorting:
                IV = [[abs(v),abs(i)] for v,i in zip(bias_v,current_v)]
                IV.sort()

                iv_in_spec = False
                if step == 'Bonded':
                    #Get highest voltage reached:
                    v_max = IV[-1][0]
                    if v_max < 325:
                        report_message.append(f":warning: IV didn't reach 325V ({v_max}), marking curve as bad.")
                    else:
                        I_max = 0
                        V_Imax = 0
                        for (vv,ii) in IV:
                            if vv < 100:
                                ### There are often outliers at low voltage, ignoring them
                                continue
                            if ii > I_max:
                                I_max = ii
                                V_Imax = vv
                        if I_max > 1000:
                            report_message.append(f":warning: IV is above specs! I max of {I_max} (> 1000 nA) at V = {V_Imax}")
                        iv_in_spec = (I_max <= 1000)


                
                elif step =='Final':
                    #Get highest voltage reached:
                    v_max = IV[-1][0]
                    if v_max < 750:
                        report_message.append(f":warning: IV didn't reach 750 ({v_max}), marking curve as bad.")
                    else:
                        I_max = 0
                        V_Imax = 0
                        for (vv,ii) in IV:
                            if vv < 100:
                                ### There are often outliers at low voltage, ignoring them
                                continue
                            if ii > I_max:
                                I_max = ii
                                V_Imax = vv
                        if I_max > 5000:
                            report_message.append(f":warning: IV is above specs! I max of {I_max} (> 5000 nA) at V = {V_Imax}")
                        else:
                            ### Checking the slope
                            ### We want the first point before 600V to be < 2.5*max
                            v_600 = 0
                            i_600 = 0
                            for (vv,ii) in IV:
                                if vv < 600:
                                    v_600 = vv
                                    i_600 = ii
                                else:
                                    break
                            if I_max > i_600  * 2.5:
                                report_message.append(f":warning: IV slope is not in spec!!! I = {I_max} at V = {V_Imax}, which is more than i_600*2.5 = {i_600}/2.5")
                            else:
                                iv_in_spec = True
                    

                else:
                    report_message.append(f":warning: Unknown step {step}. IV will be considered as bad by default.")
                    alert_message.append(f":warning: Unknown step {step}. IV will be considered as bad by default.")

                self.iv_data = {}
                self.iv_sub_data = {}
                self.iv_data = {
                    'part_key': auto_widget_dic["part_field"].text(), 
                    'test_type_id': 1,
                    'test_station_key' : 'RO1',
                    'in_spec': int(iv_in_spec),
                    'measured_at': timestamp_to_str(test_time),
                }
                self.iv_sub_data = {
                    'construction_step': step,
                    'operator': auto_widget_dic["operator_field"].text(),
                    'measurement': json.dumps({
                        'Bias [V]' : bias_v,
                        'Current [nA]' : current_v,
                        'TEMP1 [C]' : temp1_v,
                        'TEMP2 [C]' : temp2_v,
                        'HUMI [%]' : humi_v,
                        'LUMI' : lumi_v,
                    }),
                    'comments': self.user_comment.toPlainText()
                }
            except Exception as e:
                alert_message.append(f"{e}")
                return 0, alert_message, report_message

            if iv_in_spec:
                report_message.append(":white_check_mark: GOOD IV curve")
            else:
                report_message.append(":x: BAD IV curve. @delcourt, please investigate")
                alert_message.append("WARNING, IV CURVE IS NOT IN SPEC")
            return 1, alert_message, report_message
        except Exception as e:
            print("Unable to generate gabriel dic")
            alert_message.append(f"Unable to generate gabriel dic: {e}")
            return 0,alert_message, report_message



if __name__ == "__main__":
    gg = gui()


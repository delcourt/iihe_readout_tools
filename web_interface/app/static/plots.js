var T0 = 273.5;
var C  = 2.216679;
var a  = 611.6441;
var m  = 7.591386;

function get_abs(h,T){
  return(C*1.0/(T+T0)*a*Math.pow(10,(m*T*1./(T+T0)))*h*0.01);
 }

function get_graph(dt = 0){
    document.getElementById("generate_graph").disabled = true;
    url="plotting_data"
    if (dt ==0){
      var start_time=document.getElementById("start_year").value+"_"
                    +document.getElementById("start_month").value+"_"
                    +document.getElementById("start_day").value+"_"
                    +document.getElementById("start_hour").value+"_"
                    +document.getElementById("start_min").value+"_00";

      url+="?start="+start_time;

      var stop_time =document.getElementById("stop_year").value+"_"
                    +document.getElementById("stop_month").value+"_"
                    +document.getElementById("stop_day").value+"_"
                    +document.getElementById("stop_hour").value+"_"
                    +document.getElementById("stop_min").value+"_00";
      url+="&stop="+stop_time;
    }
    else{
      url+="?start="+60*60*dt;
    }
    url+="&max_points="+document.getElementById("N_points").value;
    url+="&data_file="+document.getElementById("data_file").value;
    xmlhttp=new XMLHttpRequest();
    xmlhttp.onreadystatechange=function()
    {
      if (xmlhttp.readyState==4 && xmlhttp.status==200){
            var data = JSON.parse(xmlhttp.responseText);
            
            var layout_temp = {
                title: {
                  text:'Temperature',
                },
                yaxis: {
                  title: {
                    text: 'Temperature [°C]',
                  }
                }
              };

            
            Plotly.newPlot('plot_temp', data["temp"],layout_temp);

            var layout_hum = {
                title: {
                  text:'Humidity',
                },
                yaxis: {
                  title: {
                    text: '% relative humidity',
                  }
                }
              };

              
            Plotly.newPlot('plot_hum', data["hum"],layout_hum);
            document.getElementById("generate_graph").disabled = false;

        }
    }
    document.getElementById('plot_temp').innerHTML = "";
    document.getElementById('plot_hum').innerHTML = "";
    document.getElementById('plot_hum_abs').innerHTML = "";
    xmlhttp.open("GET", url , false );
    xmlhttp.send(null);
}

  
get_graph();

var last_refresh = -1;
function auto_refresh(){
  var d = new Date();
  var n = d.getTime(); 
  if (last_refresh == -1){
    last_refresh = d.getTime();
  }
  var dt = (n-last_refresh)/1000;
  if (dt < 60){
    document.getElementById("last_refresh").innerHTML = "Last refresh : "+Math.floor(dt)+"s ago";
  }
  else{
    document.getElementById("last_refresh").innerHTML = "Last refresh : "+Math.floor(dt/60)+"m ago";
  }
  if (dt > 60*5 && document.getElementById("auto_refresh").checked){
    location.reload(true); 
  }
}

auto_refresh();
window.setInterval(auto_refresh,1000);

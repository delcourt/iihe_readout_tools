
/*To implement in js:
stop_iv()
clear_error()
download_raw()
download_processed()
*/
var first_plot = true;
var prev_state = "not_existing";

var old_links = [];

function update(){
    url="iv_data"

    xmlhttp=new XMLHttpRequest();
    xmlhttp.onreadystatechange=function()
    {
      if (xmlhttp.readyState==4 && xmlhttp.status==200){
            var reply = JSON.parse(xmlhttp.responseText);
            var state   = reply["status"][0];
            var message = reply["status"][1];
            var links   = reply["f_list"];
            console.log(links.length);
            document.getElementById("iv_status").innerHTML = state;
            document.getElementById("iv_message").innerHTML = message;
            var layout = {
                title: {
                  text:'IV curve',
                },
                yaxis: {
                  title: {
                    text: 'Current [µA]',
                  }
                }
              };

            
            console.log("Updating...")
            if (first_plot == true){
              Plotly.newPlot('plot', [reply["data"]],layout);
              first_plot = false;
            }
            else{
              if (state=="running" || prev_state == "running"){
                Plotly.react('plot', [reply["data"]],layout);
              }
            }

            //Changing buttons...
            if (prev_state != state){
              if (state == "running"){
                document.getElementById("start_iv").disabled=true;
                document.getElementById("calibrate").disabled=true;
                document.getElementById("stop_iv").disabled=false;
                  document.getElementById("clear_error").disabled=true;
              }
              else if (state == "ready"){
                document.getElementById("start_iv").disabled=false;
                document.getElementById("calibrate").disabled=false;
                document.getElementById("stop_iv").disabled=true;
                document.getElementById("clear_error").disabled=true;
              }
              else if (state == "error"){
                document.getElementById("start_iv").disabled=true;
                document.getElementById("calibrate").disabled=true;
                document.getElementById("stop_iv").disabled=true;
                document.getElementById("clear_error").disabled=false;
              }
              else if (state == "locked"){
                document.getElementById("start_iv").disabled=true;
                document.getElementById("calibrate").disabled=true;
                document.getElementById("stop_iv").disabled=true;
                document.getElementById("clear_error").disabled=true;
              }
            }
            prev_state = state;

            //Changing links...
            var link_div = document.getElementById("link_div");
            // console.log(old_links[0])
            if (links.length == 0)
              link_div.innerHTML = "";
            if (old_links.length == 0 || old_links[0] != links[0]){
              console.log(old_links[0])
              console.log(links[0])
              link_div.innerHTML = "";
              new_html = "<ul>";
              for (var elemId = 0; elemId < links.length; elemId ++){
                  new_html+="<li><a href='/iv_results/"+links[elemId]+"'>"+links[elemId]+"</a></li>";
              }
              new_html+="</ul>";
              old_links = links;
            }
            
            link_div.innerHTML = new_html;

            document.getElementById("correction_label").innerHTML=reply["correction"];
        }
    }
    xmlhttp.open("GET", url , false );
    xmlhttp.send(null);
}

  
function start_iv(){
  document.getElementById("start_iv").disabled=true;
  document.getElementById("calibrate").disabled=true;

  url="iv_command?cmd=start";
  url+="&v_start="+document.getElementById("iv_v0").value;
  url+="&v_stop="+document.getElementById("iv_v1").value;
  url+="&v_step="+document.getElementById("iv_vstep").value;
  url+="&t_idle="+document.getElementById("iv_t_idle").value;
  xmlhttp=new XMLHttpRequest();
  xmlhttp.open("GET", url , false );
  xmlhttp.send(null);
}

function calibrate(){
  document.getElementById("start_iv").disabled=true;
  document.getElementById("calibrate").disabled=true;

  url="iv_command?cmd=calibrate";
  url+="&v_start="+document.getElementById("iv_v0").value;
  url+="&v_stop="+document.getElementById("iv_v1").value;
  url+="&v_step="+document.getElementById("iv_vstep").value;
  url+="&t_idle="+document.getElementById("iv_t_idle").value;
  xmlhttp=new XMLHttpRequest();
  xmlhttp.open("GET", url , false );
  xmlhttp.send(null);
}



function stop_iv(){
  document.getElementById("stop_iv").disabled=true;
  url="iv_command?cmd=stop";
  xmlhttp=new XMLHttpRequest();
  xmlhttp.open("GET", url , false );
  xmlhttp.send(null);
}

function clear_calibration(){
  if (confirm("Are you sure you want to clear the calibration?")){
    url="iv_command?cmd=clear_calibration";
    xmlhttp=new XMLHttpRequest();
    xmlhttp.open("GET", url , false );
    xmlhttp.send(null);
  }
}



function clear_error(){
  document.getElementById("clear_error").disabled=true;
  prev_state = "new";
  url="iv_command?cmd=clear_error";
  xmlhttp=new XMLHttpRequest();
  xmlhttp.open("GET", url , false );
  xmlhttp.send(null);
}

function get_config_list(){
  var e = document.getElementById("config_file_select");
  var text = e.options[e.selectedIndex].text;
  url="get_hv_config_list?file="+text
  enable_load();

  xmlhttp=new XMLHttpRequest();
  xmlhttp.onreadystatechange=function()
  {
    if (xmlhttp.readyState==4 && xmlhttp.status==200){

          var calib_select = document.getElementById("calib_select");
          calib_select.innerHTML = "";
          var reply = JSON.parse(xmlhttp.responseText);
          console.log(reply);
          var selected = reply["selected"];
          for (const key in reply["calibrations"]) {

            console.log(key);
            var opt = document.createElement('option');
            opt.value = key;
            opt.innerHTML = key;
            if (key==selected)
              opt.selected = true;
            calib_select.appendChild(opt);
        }          
      }
  }
  xmlhttp.open("GET", url , false );
  xmlhttp.send(null);
}

function enable_load(){
  document.getElementById("load_correction").disabled = false;
}

function load_correction(){
  if (confirm("Are you sure you want to load a new HV calibration?") == 0)
      return 0;
  
  document.getElementById("load_correction").disabled = true;

  var e = document.getElementById("config_file_select");
  var text = e.options[e.selectedIndex].text;
  var e2 = document.getElementById("calib_select");
  var calib = e2.options[e2.selectedIndex].text;
  url="set_hv_calib?file="+text+"&calib="+calib;

  xmlhttp=new XMLHttpRequest();
  xmlhttp.onreadystatechange=function()
  {
    if (xmlhttp.readyState==4 && xmlhttp.status==200){
        //Nothing to do?
        console.log(xmlhttp.responseText)
      }
  }
  xmlhttp.open("GET", url , false );
  xmlhttp.send(null);
}
// update();

// var last_refresh = -1;
// function auto_refresh(){
//   var d = new Date();
//   var n = d.getTime(); 
//   if (last_refresh == -1){
//     last_refresh = d.getTime();
//   }
//   var dt = (n-last_refresh)/1000;
//   if (dt < 60){
//     document.getElementById("last_refresh").innerHTML = "Last refresh : "+Math.floor(dt)+"s ago";
//   }
//   else{
//     document.getElementById("last_refresh").innerHTML = "Last refresh : "+Math.floor(dt/60)+"m ago";
//   }
//   if (dt > 60*5 && document.getElementById("auto_refresh").checked){
//     location.reload(true); 
//   }
// }
document.getElementById('plot').innerHTML = "";
update();
window.setInterval(update,1000);


var T0 = 273.5;
var C  = 2.216679;
var a  = 611.6441;
var m  = 7.591386;''

function get_abs(h,T){
  return(C*1.0/(T+T0)*a*Math.pow(10,(m*T*1./(T+T0)))*h*0.01);
 }

function get_graph(dt = 0){
document.getElementById("generate_graph").disabled = true;
url="plotting_data_psu"
if (dt ==0){
    var start_time=document.getElementById("start_year").value+"_"
                +document.getElementById("start_month").value+"_"
                +document.getElementById("start_day").value+"_"
                +document.getElementById("start_hour").value+"_"
                +document.getElementById("start_min").value+"_00";

    url+="?start="+start_time;

    var stop_time =document.getElementById("stop_year").value+"_"
                +document.getElementById("stop_month").value+"_"
                +document.getElementById("stop_day").value+"_"
                +document.getElementById("stop_hour").value+"_"
                +document.getElementById("stop_min").value+"_00";
    url+="&stop="+stop_time;
}

else{
    url+="?start="+60*60*dt;
    }
    url+="&max_points="+document.getElementById("N_points").value;
    url+="&data_file="+document.getElementById("data_file").value;
    xmlhttp=new XMLHttpRequest();
    xmlhttp.onreadystatechange=function()
    {
    if (xmlhttp.readyState==4 && xmlhttp.status==200){
            var data = JSON.parse(xmlhttp.responseText);

            var layout_temp = {
                title: {
                text:'Voltage',
                },
                yaxis: {
                title: {
                    text: 'Volts',
                }
                }
            };


            Plotly.newPlot('plot_lv_v', data["lv_v"],layout_temp);

            var layout_hum = {
                title: {
                    text:'Current',
                },
                yaxis: {
                    title: {
                    text: 'Amps',
                    }
                }
                };


            Plotly.newPlot('plot_lv_i', data["lv_i"],layout_hum);
            document.getElementById("generate_graph").disabled = false;


            var layout_hv_v = {
                title: {
                text:'High voltage',
                },
                yaxis: {
                title: {
                    text: 'V',
                }
                }
            };

            Plotly.newPlot('plot_hv_v', data["hv_v"],layout_hv_v);
            document.getElementById("generate_graph").disabled = false;

            var layout_hv_i = {
            title: {
                text:'High voltage current',
            },
            yaxis: {
                title: {
                text: 'I [µA]',
                }
            }
            };


            Plotly.newPlot('plot_hv_i', data["hv_i"],layout_hv_i);
            document.getElementById("generate_graph").disabled = false;

        }
    }
    document.getElementById('plot_lv_v').innerHTML = "";
    document.getElementById('plot_lv_i').innerHTML = "";
    document.getElementById('plot_hv_v').innerHTML = "";
    document.getElementById('plot_hv_i').innerHTML = "";

    xmlhttp.open("GET", url , false );
    xmlhttp.send(null);
}


get_graph();

var last_refresh = -1;
function auto_refresh(){
var d = new Date();
var n = d.getTime();
if (last_refresh == -1){
last_refresh = d.getTime();
}
var dt = (n-last_refresh)/1000;
if (dt < 60){
document.getElementById("last_refresh").innerHTML = "Last refresh : "+Math.floor(dt)+"s ago";
}
else{
document.getElementById("last_refresh").innerHTML = "Last refresh : "+Math.floor(dt/60)+"m ago";
}
if (dt > 60*5 && document.getElementById("auto_refresh").checked){
location.reload(true);
}
}

auto_refresh();
window.setInterval(auto_refresh,1000);

from app import app
from flask import render_template, request
import subprocess, json
from datetime import datetime, timedelta
import os,sys,math, glob, json
from magnet_db import event, magnet_db
from dummy_db import dummy_db
from zmq_interface import zmq_client
rq = None #zmq_client(5555)
from flask import send_from_directory
import time

WEB_DIR = os.environ["WEB_DIR"]
IIHE_TOOLS_BASE_DIR=os.environ["IIHE_TOOLS_BASE_DIR"]

config_file   = IIHE_TOOLS_BASE_DIR+"/common/config/web_app.json"
rht_files_dir = IIHE_TOOLS_BASE_DIR+"/rht_gui/"
psu_files_dir = IIHE_TOOLS_BASE_DIR+"/psu_control/"
st_files_dir  = IIHE_TOOLS_BASE_DIR+"/sensortag/"


psu_naming ={
             "LV1":"CH1 (DUT)",
             "LV2":"CH2",
             "LV3":"CH3",
             "HV":"DUT",
             "LI1":"CH1 (DUT)",
             "LI2":"CH2",
             "LI3":"CH3",
             "HI":"DUT",
             }




def time_select_blocks(start,stop):
    now = datetime.now()
    yesterday = now +timedelta(hours=-2)
    months = ["January","February","March","April","May","June","July","August","September","October","November","December"]
    data = {}
    for block,tt in [("start",yesterday),("stop",now)]:
        data[block] = f"<select id='{block}_year'>"
        for year in reversed(range(2020, now.year+1)):
            data[block]+=f"<option>{year}</option>"#<option>2021</option><option>2020</option></select>"
        data[block]+="</select>"
        data[block]+= f"<select id='{block}_month'>"
        for i in range(12):
            if i+1 == tt.month:
                data[block]+=f"<option value='{i+1}' selected>{months[i]}</option>"
            else:
                data[block]+=f"<option value='{i+1}'>{months[i]}</option>"

        data[block]+=f"</select><select id='{block}_day'>"
        for i in range(31):
            if i+1 == tt.day:
                data[block]+=f"<option value='{i+1}' selected>{i+1:02d}</option>"
            else:
                data[block]+=f"<option value='{i+1}' >{i+1:02d}</option>"

        data[block]+=f"</select><select id='{block}_hour'>"
        for i in range(24):
            if i == tt.hour:
                data[block]+=f"<option value='{i}' selected>{i:02d}h</option>"
            else:
                data[block]+=f"<option value='{i}' >{i:02d}h</option>"
        data[block]+=f"</select><select id='{block}_min'>"
        for i in range(60):
            if i == tt.minute:
                data[block]+=f"<option value='{i}' selected>{i:02d}</option>"
            else:
                data[block]+=f"<option value='{i}'>{i:02d}</option>"
        data[block]+="</select>"
    return data





@app.route('/')
@app.route('/index')

def index():
    start = request.args.get('start')
    stop = request.args.get('stop')
    data = time_select_blocks(start,stop)
    fList = sorted(glob.glob(rht_files_dir+'*.bin'), key=os.path.getmtime)
    fList.reverse()
    files = [f.split("/")[-1] for f in fList]
    return render_template('plots.html',start_block = data["start"], stop_block=data["stop"], file_list = files)


@app.route('/plotting_data')
def env_data():
    conf = json.load(open(config_file,"r"))
    debug_message = ""
    start = request.args.get('start')
    if not start == None and start.isdigit():
        start = datetime.now().timestamp() - int(start)
    else:
        try:
            start = datetime.strptime(start, '%Y_%m_%d_%H_%M_%S').timestamp()
        except Exception as e:
            start = datetime.now().timestamp() - 2*60*60

    stop = request.args.get('stop')
    try:
        stop = datetime.strptime(stop, '%Y_%m_%d_%H_%M_%S').timestamp()
    except Exception as e:
        stop = datetime.now().timestamp()
    print(start,stop)
    max_points = request.args.get('max_points')
    if max_points == None or not max_points.isdigit():
        max_points = 1000
    else:
        max_points = int(max_points)
    data_file = rht_files_dir+request.args.get('data_file')
    data = {"time":[], "temp" : {}, "hum" : {}}
    print(data)
    db = magnet_db(config_file,data_file)

    #Find starting point :
    n_evt = db.number_of_events
    print(n_evt)
    n_bits = int(math.log(n_evt)/math.log(2)) + 1

    entry_number_start = 0
    entry_number_stop  = 0

    for b in reversed(range(n_bits)):
        to_check = entry_number_start + (1<<b)
        if to_check < n_evt and db.get_entry(to_check)["timestamp"] < start:
            print(db.get_entry(to_check))
            entry_number_start = to_check
        to_check = entry_number_stop + (1<<b)
        if to_check < n_evt and db.get_entry(to_check)["timestamp"] < stop:
            entry_number_stop = to_check
    debug_message+=f"Start dt : {db.get_entry(entry_number_start)['timestamp']-start} <br/>"
    debug_message+=f"Stop  dt : {db.get_entry(entry_number_stop)['timestamp']-stop} <br/>"
    print(debug_message)
    #print (debug_message)

    read_every = 1
    while (entry_number_stop-entry_number_start)*1./read_every > max_points:
        read_every += 1

    entry_number = entry_number_start
    while(entry_number < entry_number_stop):
        entry = db.get_entry(entry_number)
        entry_number+=read_every
        if not entry==None:
            if entry.data["timestamp"] < start or entry.data["timestamp"] > stop:
                continue
            for key in entry.data:
                val = entry.data[key]

                if key == "timestamp":
                    data["time"].append(datetime.fromtimestamp(val).strftime("%Y-%m-%d %H:%M:%S"))
                if not key in conf["meas_to_show"]:
                    continue

                data_type = "None"
                if  "_T" in key or "PT_" in key:
                    data_type = "temp"
                elif  "_H" in key:
                    data_type = "hum"
                if data_type == "None":
                    continue

                if key in conf["alias"].keys():
                    key = conf["alias"][key]


                if not key in data[data_type]:
                    data[data_type][key] = [val]
                else:
                    if data_type == "temp" and val == -50.0:
                        data[data_type][key].append(data[data_type][key][-1])
                    else:
                        data[data_type][key].append(val)

        entry_number += read_every
    print(data)
    #creating plots from data :
    plots = {"temp" : [], "hum" : []}
    for meas_type in ["temp","hum"]:
        for meas in data[meas_type]:
            plots[meas_type].append({
                "y" : data[meas_type][meas],
                "x" : data["time"],
                "type" : "scatter",
                "name" : meas
            })
    print(plots)
    return plots
@app.route('/psu')
def psu():
    start = request.args.get('start')
    stop = request.args.get('stop')
    data = time_select_blocks(start,stop)
    #fList = sorted(glob.glob(psu_files_dir+'*.bin'), key=os.path.getmtime)
    #fList.reverse()
    #files = [f.split("/")[-1] for f in fList]
    return render_template('plots_psu.html',start_block = data["start"], stop_block=data["stop"])

def val_converter(meas_type,val):
    if "lv" in meas_type or "hv_v" in meas_type:
        return 0.001*val
    if "hv_i" in meas_type:
        return 0.001*val
    if meas_type == "temp":
        val = val/65536.0*165-40
        val = 0.1*int(10*val+0.5)
        return val
    if meas_type == "hum":
        val = val/ 65536.0 * 100
        val = 0.1*int(10*val+0.5)
        return val

@app.route('/plotting_data_psu')
def plotting_data_psu():
    start = request.args.get('start')
    if not start == None and start.isdigit():
        start = datetime.now().timestamp() - int(start)
    else:
        try:
            start = datetime.strptime(start, '%Y_%m_%d_%H_%M_%S').timestamp()
        except Exception as e:
            start = datetime.now().timestamp() - 2*60*60

    stop = request.args.get('stop')
    try:
        stop = datetime.strptime(stop, '%Y_%m_%d_%H_%M_%S').timestamp()
    except Exception as e:
        stop = datetime.now().timestamp()

    max_points = request.args.get('max_points')
    if max_points == None or not max_points.isdigit():
        max_points = 1000
    else:
        max_points = int(max_points)

    #data_file = files_dir+request.args.get('data_file')
    data_file = psu_files_dir+"/psu.db"
    data = {"date":[], "lv_v" : {}, "lv_i" : {}, "hv_v" : {}, "hv_i" : {}}

    db = dummy_db(data_file)

    #Find starting point :
    n_evt = db.number_of_events
    n_bits = int(math.log(n_evt)/math.log(2)) + 1

    entry_number_start = 0
    entry_number_stop  = 0

    for b in reversed(range(n_bits)):
        to_check = entry_number_start + (1<<b)
        if to_check < n_evt and db.get_entry(to_check)["date"] < start:
            entry_number_start = to_check
        to_check = entry_number_stop + (1<<b)
        if to_check < n_evt and db.get_entry(to_check)["date"] < stop:
            entry_number_stop = to_check


    read_every = 1
    while (entry_number_stop-entry_number_start)*1./read_every > max_points:
        read_every += 1

    entry_number = entry_number_start
    while(entry_number < entry_number_stop):
        entry = db.get_entry(entry_number)
        entry_number+=read_every
        if not entry==None:            
            if entry["date"] < start or entry["date"] > stop:
                continue

            for key in entry:
                val = entry[key]

                if key == "date":
                    data["date"].append(datetime.fromtimestamp(val).strftime("%Y-%m-%d %H:%M:%S"))

                data_type = "None"
                if  "LV" in key:
                    data_type = "lv_v"
                elif  "LI" in key:
                    data_type = "lv_i"
                elif  "HV" in key:
                    data_type = "hv_v"
                elif  "HI" in key:
                    data_type = "hv_i"
                if data_type == "None":
                    continue

                if not key in data[data_type]:
                    data[data_type][key] = [val_converter(data_type,val)]
                else:
                    data[data_type][key].append(val_converter(data_type,val))

        # entry_number += read_every

    #creating plots from data :
    plots = {"lv_v" : [], "lv_i" : [],"hv_v" : [], "hv_i" : []}
    for meas_type in ["lv_v","lv_i","hv_v","hv_i"]:
        for meas in data[meas_type]:
            name = meas
            if meas in psu_naming.keys():
                name = psu_naming[meas]
            if name == "N/A":
                continue
            plots[meas_type].append({
                "y" : data[meas_type][meas],
                "x" : data["date"],
                "type" : "scatter",
                "name" : name
            })
    return plots


@app.route('/psu_control')
def psu_control_page():
    return render_template('psu_control_page.html')

@app.route('/get_live_psu')
def get_live_psu():
    global rq
    try:
        if rq == None:
            print("Starting zmq client")
            rq = zmq_client(5555)

        res_0 = rq.query("LV get_measurements")
        res_1 = rq.query("HV get_measurements")
        print("DEBUG : ",res_0, res_1)
        res_0 = json.loads(res_0)
        res_1 = json.loads(res_1)
        data = {}
        # DEBUG :  {"LV1": 0.0, "LI1": 0.0, "LV2": 0.0, "LI2": 0.0, "LV3": 0.0, "LI3": 0.0} {"HV": 1.0, "HI": 0.148}
        data["1_status"]  = rq.query("LV is_enabled 1")
        data["1_set_v"]   =  float(rq.query("LV get_v_set 1"))
        data["1_mon_v"]   =  res_0["LV1"] #float(rq.query("LV get_voltage 1"))
        data["1_mon_i"]   =  res_0["LI1"] #float(rq.query("LV get_current 1"))
        data["2_status"]  = rq.query("LV is_enabled 2")
        data["2_set_v"]   =  float(rq.query("LV get_v_set 2"))
        data["2_mon_v"]   =  res_0["LV2"] #float(rq.query("LV get_voltage 2"))
        data["2_mon_i"]   =  res_0["LI2"] #float(rq.query("LV get_current 2"))
        data["3_status"]  = rq.query("LV is_enabled 3")
        data["3_set_v"]   =  float(rq.query("LV get_v_set 3"))
        data["3_mon_v"]   =  res_0["LV3"] #float(rq.query("LV get_voltage 3"))
        data["3_mon_i"]   =  res_0["LI3"] #float(rq.query("LV get_current 3"))
#        data["hv_status"] = rq.query("1 is_enabled")
        data["hv_set_v"]  =  float(rq.query("HV get_v_set"))
        data["hv_mon_v"]  =  res_1["HV"] #float(rq.query("HV get_voltage"))
        data["hv_mon_i"]  =  res_1["HI"] #float(rq.query("HV get_current"))
        hv_st = json.loads(rq.query("HV get_status_list").replace("'",'"'))
        if len(hv_st) == 0:
            hv_st = ["ERR", "NON EXISTANT"]
        data["hv_status"] = (hv_st[0] == "ON")
        data["hv_ramp"]   = ""
        if "RAMP UP" in hv_st:
            data["hv_ramp"] += "Ramp up "
            hv_st.remove("RAMP UP")
        if "RAMP DOWN" in hv_st:
            data["hv_ramp"] += "Ramp down "
            hv_st.remove("RAMP DOWN")    
        data["hv_error"] = ";".join(hv_st[1:])
        
        return data
    except Exception as e:
        print(f"Error : {e}")
        rq = None
        return []

@app.route("/send_psu_cmd")
def send_psu_cmd():
    global rq
    try:
        if rq == None:
            print("Starting zmq client")
            rq = zmq_client(5555)
        cmd = request.args.get('cmd')
        if cmd == None:
            return ("No command to process")
        
        cmd = cmd.split(" ")
        if cmd[0] in ["turn_on","turn_off"]:
            if len(cmd) == 2:
                if cmd[1].isdigit() and int(cmd[1]) in [1,2,3]:
                    rq.query(f"LV {cmd[0]} {cmd[1]}")
                elif cmd[1] == "hv":
                    rq.query(f"HV {cmd[0]}")
                    if cmd[0] == "turn_on":
                        rq.query("led set_state locked")

        elif cmd[0] == "set_v":
            print(cmd)
            if len(cmd) == 3:
                try:
                    channel = cmd[1]
                    voltage = float(cmd[2])
                    if channel in ["1","2","3"] and voltage >= 0 and voltage <= 15:
                        rq.query(f"LV set_v {voltage} {channel}")
                    elif channel == "hv":
                        print("changing HV voltage")
                        rq.query(f"HV set_v {voltage}")
                except Exception as e:
                    print(e)
                    pass
        elif cmd[0] == "restart":
            rq.query("restart_service")
            print("Restated command launched... Starting new client :")
            rq = zmq_client(5555)
            print("Done !")
        return ""
    except Exception as e:
        print("Error in request, stopping zmq client...")
        rq = None



@app.route('/sensor_tag')
def sensor_tag():
    start = request.args.get('start')
    stop = request.args.get('stop')
    data = time_select_blocks(start,stop)
    #fList = sorted(glob.glob(files_dir+'*.bin'), key=os.path.getmtime)
    #fList.reverse()
    #files = [f.split("/")[-1] for f in fList]
    return render_template('sensor_tag.html',start_block = data["start"], stop_block=data["stop"])


@app.route('/sensor_tag_data')
def sensor_tag_data():
    start = request.args.get('start')
    if not start == None and start.isdigit():
        start = datetime.now().timestamp() - int(start)
    else:
        try:
            start = datetime.strptime(start, '%Y_%m_%d_%H_%M_%S').timestamp()
        except Exception as e:
            start = datetime.now().timestamp() - 2*60*60

    stop = request.args.get('stop')
    try:
        stop = datetime.strptime(stop, '%Y_%m_%d_%H_%M_%S').timestamp()
    except Exception as e:
        stop = datetime.now().timestamp()

    max_points = request.args.get('max_points')
    if max_points == None or not max_points.isdigit():
        max_points = 1000
    else:
        max_points = int(max_points)

    #data_file = files_dir+request.args.get('data_file')
    data_file = st_files_dir+"/data.db"
    data = {"date":[], "temp" : {}, "hum" : {}}

    db = dummy_db(data_file)
    print(db, data_file)
    #Find starting point :
    n_evt = db.number_of_events
    n_bits = int(math.log(n_evt)/math.log(2)) + 1

    entry_number_start = 0
    entry_number_stop  = 0

    for b in reversed(range(n_bits)):
        to_check = entry_number_start + (1<<b)
        if to_check < n_evt and db.get_entry(to_check)["date"] < start:
            entry_number_start = to_check
        to_check = entry_number_stop + (1<<b)
        if to_check < n_evt and db.get_entry(to_check)["date"] < stop:
            entry_number_stop = to_check


    read_every = 1
    while (entry_number_stop-entry_number_start)*1./read_every > max_points:
        read_every += 1

    entry_number = entry_number_start
    while(entry_number < entry_number_stop):
        entry = db.get_entry(entry_number)
        if not entry==None:
            for key in entry:
                val = entry[key]

                if key == "date":
                    data["date"].append(datetime.fromtimestamp(val).strftime("%Y-%m-%d %H:%M:%S"))

                data_type = "None"
                if key.startswith("T_"):
                    data_type = "temp"
                elif key.startswith("H_"):
                    data_type = "hum"
                if data_type == "None":
                    continue

                if not key in data[data_type]:
                    data[data_type][key] = [val_converter(data_type,val)]
                else:
                    data[data_type][key].append(val_converter(data_type,val))

        entry_number += read_every

    #creating plots from data :
    plots = {"temp" : [], "hum" : []}
    for meas_type in ["temp","hum"]:
        for meas in data[meas_type]:
            name = meas[2:]
            plots[meas_type].append({
                "y" : data[meas_type][meas],
                "x" : data["date"],
                "type" : "scatter",
                "name" : name
            })
    return plots 


@app.route('/iv_curve')

def iv_curve():
    # global rq
    # if rq == None:
    #     print("Starting zmq client")
    #     rq = zmq_client(5555)
    
    # status = rq.query("IV_runner get_state")
    # data   = rq.query("IV_runner get_data")
    # print(status,data)
    # return {"status":status, "data":data}

    # Change me to be in the config file
    config_home_dir = os.environ["IIHE_TOOLS_BASE_DIR"]+"/data/HV_calib/"
    calib_file_list = [f for f in  os.listdir(config_home_dir) if ".json" in f ]
    selected_file  = "None"
    selected_calib = "None"
    try:
        settings = json.load(open(os.environ["IIHE_TOOLS_BASE_DIR"]+"/data/HV_calib.json","r"))
        selected_file = settings["selected_file"]
        selected_calib = settings["selected_calib"]
    except Exception as e:
        print(f"Error: {e}")
    calibration_list = ["None"]
    try:
        settings = json.load(open(config_home_dir+selected_file, "r"))
        calibration_list = settings["calibrations"].keys()
    except Exception as e:
        print(f"Error: {e}")
    return render_template('IV_curve.html', calib_file_list = calib_file_list, selected_file = selected_file, selected_calib = selected_calib, calibration_list = calibration_list)

cached_iv_data = {"time":0}#,"status":"","data_raw":"","f_list":"", "correction":""}

@app.route('/iv_data')
def iv_data():

    global cached_iv_data
    if time.time()-cached_iv_data["time"] > 1:
        print("Old data, getting newer...")
        cached_iv_data["time"]        = time.time()
        #Mouhaha, other threads might recieve old news!
        global rq
        if rq == None:  
            print("Starting zmq client")
            rq = zmq_client(5555)    
    
        status   = rq.query("IV_runner get_state")
        data_raw = rq.query("IV_runner get_data")
        f_list   = rq.query("IV_runner get_files")
        correction = rq.query("IV_runner get_correction")
        try:
            data_raw=json.loads(data_raw)
            data={
                    "y" : [float(x[1]) for x in data_raw],
                    "x" : [float(x[0]) for x in data_raw],
                    "type" : "scatter",
                }
        except Exception as e:
            data = []

        try:
            status = json.loads(status)
        except Exception as e:
            status = ""

        try:
            # print(f_list)
            f_list = json.loads(f_list.replace("'",'"'))
        except Exception as e:
            print(e)
            f_list = []
        # cached_iv_data["status"]      = status
        # cached_iv_data["data"]        = data
        # cached_iv_data["f_list"]      = f_list
        # cached_iv_data["correction"]  = correction
    
        cached_iv_data = {"status":status, "data":data, "f_list":f_list, "correction":correction}
        cached_iv_data["time"]        = time.time()
    return cached_iv_data

@app.route('/iv_command')
def iv_command():
    try:
        cmd = request.args.get('cmd')
        if not cmd in ["start", "stop", "clear_error", "calibrate","clear_calibration"]:
            return
        global rq
        if rq == None:
            print("Starting zmq client")
            rq = zmq_client(5555)

        if cmd == "start" or cmd == "calibrate":
            v_start = float(request.args.get('v_start'))
            v_stop  = float(request.args.get('v_stop'))
            v_step  = float(request.args.get('v_step'))
            t_idle  = float(request.args.get('t_idle'))
            if v_start < 0 or v_stop < 0 or v_stop > 800 or v_step < 0:
                return "Invalid parameters"
            if cmd == "calibrate":
                rq.query(f"IV_runner start_IV {v_start} {v_stop} {v_step} {t_idle} True")
            else:
                rq.query(f"IV_runner start_IV {v_start} {v_stop} {v_step} {t_idle}")
        elif cmd == "clear_error":
            rq.query("IV_runner clear_error")
        elif cmd == "stop":
            rq.query("IV_runner abort")
        elif cmd =="clear_calibration":
            rq.query("IV_runner clear_calibration")
        return "done"
    except Exception as e:
        return e


@app.route('/iv_results/<path:filename>')
def get_IV(filename):
    return send_from_directory(IIHE_TOOLS_BASE_DIR+"/data/IV_curves/", filename)


@app.route('/get_hv_config_list')
def get_hv_config_list():
    file_name = request.args.get('file')
    if not file_name in os.listdir(os.environ["IIHE_TOOLS_BASE_DIR"]+"/data/HV_calib/"):
        return f"Cannot find {file_name} in HV calibration directory"

    content = ""
    with open(os.environ["IIHE_TOOLS_BASE_DIR"]+"/data/HV_calib/"+file_name,"r") as f:
        content = f.read()
    return(content)

@app.route("/set_hv_calib")
def set_hv_calib():
    file_name = request.args.get('file')
    calib     = request.args.get('calib')
    if not file_name in os.listdir(os.environ["IIHE_TOOLS_BASE_DIR"]+"/data/HV_calib/"):
        return "Error, cannot find file in HV calib dir"

    global rq
    if rq == None:  
        print("Starting zmq client")
        rq = zmq_client(5555)    
    msg = rq.query(f"HV load_calib {file_name} {calib}")
    conf = json.load(open(os.environ["IIHE_TOOLS_BASE_DIR"]+"/data/HV_calib.json", "r"))
    conf["selected_file"]  = file_name
    conf["selected_calib"] = calib
    with open(os.environ["IIHE_TOOLS_BASE_DIR"]+"/data/HV_calib.json", "w")  as f:
        f.write(json.dumps(conf, indent=2))
    
    return msg

import zmq, time, threading

timeout = 2 # in seconds


class zmq_runner:
  def __init__(self, main_server, port):
    print(f"Creating zmq_runner on port {port}")
    self.port = port
    self.main_server = main_server
  
  def run(self):
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    socket.bind(f"tcp://*:{self.port}")
    print(f"Starting runner on port {self.port}")
    while True:
      try:
        message = socket.recv().decode("utf-8")
        if message == "close_runner":
          socket.send(bytes("closing","utf-8"))
          return 1
        return_message = self.main_server.process_message(message)
        print(f"zmq runner {self.port} received request: {message} ------> {return_message}")
        socket.send(bytes(return_message,"utf-8"))
      except Exception as e:
        return_message = f"Error in zqm server {port}: {e}"
        print(return_message)
        socket.send(bytes(return_message,"utf-8"))
        time.sleep(0.1)
  






class zmq_server:
  def __init__(self,target_objects, port = 5555):
    print(f"Launching zmq_server on port {port}")
    self.obj   = target_objects
    self.locks = {obj:threading.Lock() for obj in target_objects}
    self.port = port
    self.ports   = [port]
    self.threads = []
    self.runners = []
    
  def run(self):
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    socket.bind(f"tcp://*:{self.port}")
    while True:
      try:
        message = socket.recv().decode("utf-8")
        print("Received request: %s" % message)
        return_message = "Available cmd: ping , new_connection"
        if message == "ping":
          return_message = "pong"
        elif message == "new_connection":
          #Generate new port here!!!
          new_port = self.ports[-1]+1
          self.ports.append(new_port)
          new_runner = zmq_runner(self,new_port)
          self.runners.append(new_runner)
          new_thread = threading.Thread(target=new_runner.run)
          self.threads.append(new_thread)
          new_thread.start()
          return_message = str(new_port)
        socket.send(bytes(return_message,"utf-8"))
      except Exception as e:
        print(f"Error in zqm server: {e}")
        time.sleep(0.1)

  def process_message(self,message):
    print("Message = ",message)
    try:
      if message.lower() == "ping":
          return("pong")
      elif message.lower() == "list_objects":
          return(f"{self.obj}")
      elif message.split(" ")[0] in self.obj.keys():
          message = message.split(" ")
          target_obj = self.obj[message[0]]
          print("Message = ",message)
          if len(message) == 1:
            return("pong")
          elif message[1] in dir(target_obj):#.__class__.__dict__.keys():
            with self.locks[message[0]]:
              if len(message) == 2:
                return str(target_obj.__getattribute__(message[1])())
              else:
                return str(target_obj.__getattribute__(message[1])(*message[2:]))
          else:
            return(f"error, {message[1]} not a method of {message[0]}")
      else: 
        return(f"error : {message.split(' ')[0]} not a recognized object label")
    except Exception as e:
      return(f"error : {e}")

class zmq_client:
    def __init__(self,port = 5555, verbose = False):
      #Getting port:
      self.lock   = threading.Lock()
      self.client = None
      try:
        new_connection = zmq_client_base(port,verbose)
        new_port = new_connection.query("new_connection")
        self.client = zmq_client_base(new_port,verbose)
      except Exception as e:
        print("Unable to connect to server!")
        self.client = None

    def is_connected(self):
      if self.client == None:
        return False
      return self.client.connected
    
    def query(self,message):
      with self.lock:
        if self.client == None:
          print("Error, connection not established")
          return ""
        else:
          return self.client.query(message)


class zmq_client_base:
    def __init__(self, port = 5555, verbose = False):
      context = zmq.Context()
      self.stored_context = context
      context.RCVTIMEO = 1000
      if verbose:
        print(f"Connecting to zmq server at port {port}…")
      self.socket = context.socket(zmq.REQ)
      self.socket.connect(f"tcp://localhost:{port}")
      self.socket.setsockopt (zmq.LINGER, 0 )
      self.connected = False
      try:
        self.socket.send(bytes("ping",'utf-8'))
      except Exception as e:
        if verbose:
          print(f"Unable to send ping command... {e}")
        self.socket = None
        return None
      try:        
        answer = self.socket.recv()
        self.connected = (answer.decode("utf-8") == "pong")
      except Exception as e:
        if verbose:
          print(f"Unable to connect : {e}")
        self.socket = None

      if not self.connected:
        print(f"Error, unable to connect to port {port}")

    def is_connected(self):
      return self.connected

    def query(self,message):
        if self.socket == None:
          print("Error, socket not available")
          return None
        try:
          start_time = time.time()
          sent = False
          while sent == False and time.time()-start_time < timeout:
            try:
              self.socket.send(bytes(message,'utf-8'))
              sent = True
            except Exception as e:
              print(f"Warning: unable to send query: {e}")
              time.sleep(0.01)

          if sent == False:
            print("Error, impossible to send query!")
            return None

          time.sleep(0.01)
          start_time = time.time()
          while time.time()-start_time < timeout:
            try:
              answer = self.socket.recv().decode("utf-8")
              return answer
            except Exception as e:
              print(f"Warning, unable to receive message: {e}")
              pass
          print("Error, impossible to receive message!")
          return None
        except Exception as e:
          print(f"Error in query: {e}")
          if self.socket.events:
            print(self.socket.recv().decode("utf-8"))
          


if __name__ == "__main__":
  from random import random
  class dummy_psu:
    def __init__(self, id):
      self.id = id
    def ping(self, msg = ""):
      return (f"pong {msg}")
    def two_args (self, arg1, arg2):
      return (f"{arg1} {arg2}")
    def get_voltage(self, channel):
      channel = int(channel)
      return 0.1*random() + channel

    def get_current(self, channel):
      channel = int(channel)
      return 10*random() + 100*channel

  psu_list = {"0" : dummy_psu(0)}
  
  server = zmq_server(psu_list,6555)
  th = threading.Thread(target=server.run)
  th.start()

  client = zmq_client(6555)
  print("Current = ",client.query("0 get_current 1"))
  

#!/bin/bash


sessions=`tmux ls`
if [[ $sessions != "" ]]
then 
	echo "tmux sessions are already running. If you restart them, you will have to calibrate the HV psu"
	read -p "Are you sure? [Y/N] " -n 1 -r
	echo    # (optional) move to a new line
	if [[ ! $REPLY =~ ^[Yy]$ ]]
	then
	    echo "Aborting!"
	    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
	fi	
	tmux kill-server
fi
echo "Launching sessions!"
tmux new-session -d -n "psu_hw_interface"
tmux send-keys -t "psu_hw_interface" 'cd $IIHE_TOOLS_BASE_DIR/psu_control; python3 psu_interface.py' C-m

tmux new-session -d -n "psu_logger"
tmux send-keys -t "psu_logger" ' cd $IIHE_TOOLS_BASE_DIR/psu_control; python3 logger.py' C-m

tmux new-session -d -n "web_server"
tmux send-keys -t "web_server" '$IIHE_TOOLS_BASE_DIR/web_interface/runner.sh' C-m

echo "done!"

import json, time
import threading
from zmq_interface import zmq_client
from math import sqrt
from datetime import datetime
import os

N_communication_retries = 5
ZMQ_PORT = 5555   #Port of the ZMQ HW interface service

DT       = 0.5
N_STABLE = 5
N_MAX    = 20           #Maximum retries if stable current not reached
CURR_STABLE_FRAC = 0.01

V_LIMIT = 800

def avg_std(vec):
    if len(vec) <= 2:
        return 0,0
    
    avg = sum(vec)/len(vec)
    std = sqrt(sum([(x-avg)**2 for x in vec])/(len(vec)*(len(vec)-1)))
    return avg,std

class IV_runner:
    def __init__(self):
        print("Creating IV runner instance!")
        self.state     = "new"
        self.message   = "new"
        self.data      = []
        
        self.t_start        = 0
        self.t_stop         = 0
        self.processed_data = []
        self.connected = False
        self.connection = None
        self.thread_state_machine = None
        self.thread_state_machine_running = False
        self.folder_name = ""
        self.thread_IVcurve = None
        self.thread_IVcurve_running = False
        self.current_correction_name     = "offset"
        self.current_correction          = None
        self.offset_voltage = 0

    def connect(self):
        #Connecting to hw interface service
        self.connection = zmq_client(ZMQ_PORT)
        self.connected = self.connection.is_connected()
        return self.connected


    def request(self, request):
        if not self.connected or self.connection == None:
            if not self.connect():
                print("Error, unable to connect!")
                self.state   = "error"
                self.message = "Unable to connect"
                return
        answer = None
        for i in range(N_communication_retries):
            try:
                answer =  self.connection.query(request)
                break
            except Exception as e:
                print(f"Error = {e}")
                self.connetion = None
                self.connected = False
            
        if answer == None:
            print("Error, unable to send request")
            self.state   = "error"
            self.message = "Unable to send request"
        return answer

    def run_state_machine(self):
        if self.thread_state_machine == None and self.thread_state_machine_running == False:
            self.thread_state_machine = threading.Thread(target=self.refresh)
            self.thread_state_machine.start()


    def refresh(self):
        self.thread_state_machine_running = True
        while self.thread_state_machine_running:
            hv_st = []
            try:
                hv_st = json.loads(self.request("HV get_status_list").replace("'",'"'))
            except Exception as e:
                print(e)
            if "INTERLOCK" in hv_st:
                self.state = "error"
                self.message="HV psu is interlocked"
                # return

            if self.state == "new" or self.state=="ready":
                if "ON" in hv_st or "RAMP_UP" in hv_st or "RAMP_DOWN" in hv_st:
                    self.state   = "locked"
                    self.message = "Power supply is running"
                else:
                    self.state    = "ready"
                    self.message = ""

            elif self.state == "stopping" or self.state == "locked":
                if not "ON" in hv_st and not "RAMP_UP" in hv_st and not "RAMP_DOWN" in hv_st:
                    self.state = "ready"
                    self.message = ""
            time.sleep(5)
        

    def get_state(self):
        return str([self.state, self.message]).replace("'",'"')

    def get_data(self):
        return str(self.processed_data)

    def abort(self):
        self.state = "stopping"

    def clear_error(self):
        self.state = "new"
        self.message = ""
        # self.refresh()

    
    def get_current(self):
        meas = []

        stable = False
        while stable == False:
            hv_st = json.loads(self.request("HV get_status_list").replace("'",'"'))
            stable = True
            for st in hv_st:
                if "ramp" in st.lower():
                    self.message = "Ramping..."
                    time.sleep(1)
                    stable = False
                    break
        for i in range(N_STABLE):
            x     = float(self.request("HV get_current_raw"))
            v     = float(self.request("HV get_voltage_raw"))
            v_set = float(self.request("HV get_v_set"))

            t = time.time()
            meas.append(x)
            self.raw_data.append((t,x,v,v_set))
            time.sleep(DT)
        avg, std = avg_std(meas)
        n_additional  = 0
        while std > avg*CURR_STABLE_FRAC and n_additional < N_MAX:
            x = float(self.request("HV get_current_raw"))
            v = float(self.request("HV get_voltage_raw"))
            v_set = float(self.request("HV get_v_set"))
            t = time.time()
            self.raw_data.append((t,x,v,v_set))
            meas.append(x)
            time.sleep(DT)
            meas = meas[1:]+[x]
            n_additional += 1
            avg, std = avg_std(meas)
        return(avg,std)
    

    def start_IV(self, V_min, V_max, step_size, t_idle, calibration = False):
        
        if calibration == True or calibration == "True":
            print("STARTING CALIBRATION RUN!")
            self.calibration_run = 1
            self.current_correction_name = "disabled"
        # elif calibration == "second_step":
        #     print("Continuing calibration...")
        else:
            print("NOT A CALIBRATION RUN")
            self.calibration_run = 0
        
        
        if self.thread_IVcurve == None or self.thread_IVcurve_running == False:
            try:
                self.V_max = float(V_max)
                self.step_size = float(step_size)
                self.t_idle = float(t_idle)

                self.thread_IVcurve = threading.Thread(target=self.perform_iv)
                self.thread_IVcurve.start()
            except Exception as e:
                print(e)
                return f"Unable to start IV: {e}"
            return "Starting IV"
        return "Unable to start IV... Was it already running?"


    def perform_iv(self):
        self.thread_IVcurve_running = True
        if self.calibration_run != 2:
            self.file_list = []
        #TODO: Do we want to implement non linear steps?

        #TODO: turn on the HV
        
        if self.state != "ready" and self.calibration_run!=2:
            self.message = f"IV can only start from 'ready' state. State was '{self.state}: {self.message}'"
            self.state = "error"
            print(self.state,self.message)
            return

        if self.V_max > V_LIMIT:
            self.state = "error"
            self.message= f"Voltage was chosen above limit of {V_LIMIT}V"
            print(self.state,self.message)
            return
        
        if self.step_size < 1:
            self.state = "error"
            self.message = "Minimum bias step is 1V"
            print(self.state,self.message) 
            return
        self.state = "running"
        self.message = "starting"
        self.processed_data = []
        self.raw_data = []
        finished = False
        self.request("HV turn_on")
        self.request("HV set_v 0")

        if self.calibration_run == 2:
            #Wait for the end of ramp + 30s
            ready = False
            while ready == False:
                ready = True
                hv_st = json.loads(self.request("HV get_status_list").replace("'",'"'))
                for st in hv_st:
                    if "ramp" in st.lower():
                        v_mon = float(self.request("HV get_voltage_raw"))
                        self.message=f"Ramping down for validation... V={v_mon}"
                        time.sleep(10)
                        ready = False
                        break
            self.message=f"Waiting 30s for validation..."
            time.sleep(30)


        offset_init_current = self.get_current()[0]
        self.offset_voltage      = float(self.request("HV get_voltage_raw"))
        
        V = 0
        while V <= self.V_max:
            print(V)
            self.message=f"Measuring at {V} volts / {self.V_max}..."
            # Check status
            if not self.state == "running":
                print("Status is no longer 'running'!\nExiting!")
                break  

            # Change bias
            self.request(f"HV set_v {V}")

            # Wait for no longer ramping
            hv_st = json.loads(self.request("HV get_status_list").replace("'",'"'))
            while "RAMP_UP" in hv_st or "RAMP_DOWN" in hv_st:
                print("Ramping... We sleep...")
                time.sleep(1)
                hv_st = json.loads(self.request("HV get_status_list").replace("'",'"'))
            
            # Wait for 't_idle'
            print("Sleeping!")
            time.sleep(self.t_idle)

            # Check status
            if not self.state == "running":
                print("Status is no longer 'running'!\nExiting!")
                break  

            # Get measurement
            current,d_current = self.get_current()
            v = float(self.request("HV get_voltage_raw"))
            v_meas = v-self.offset_voltage
            i_meas = current
            if self.current_correction_name == "offset":
                i_meas -= offset_init_current
            elif self.current_correction_name != "disabled":
                i_meas -= self.current_correction.Eval(v_meas)
                

            self.processed_data.append([v_meas,i_meas,d_current])
            V += self.step_size

        # print(self.raw_data)
        # Start by measuring current
        # I0 = self.get_current(dt) 
        # print(I0)
        self.store_iv()

        self.request("HV set_v 0")
        if self.calibration_run == 1:
            print("Finished first step, restarting second step!")
            self.calibration_run = 2
            self.perform_iv()
        elif self.calibration_run == 2:
            self.calibration_run = 0

        if self.calibration_run == 0:
            self.request("HV turn_off")
            self.state = "locked"
            self.thread_IVcurve_running = False

    def get_correction(self):
        if self.state == "new" or self.state=="ready":
            return self.request("HV get_calib_string")
        return str(self.current_correction_name)

    def clear_calibration(self):
        self.current_correction_name="offset"
        
    def store_iv(self):
        if self.calibration_run != 2:
            self.folder_name = os.environ["IIHE_TOOLS_BASE_DIR"]+"/data/IV_curves/IV_"+datetime.now().strftime("%Y%m%d_%H%M%S")
        # print(folder_name)
        # print(self.folder_name)
        suffix = ""
        if self.calibration_run == 1:
            suffix = "_calibration"
        elif self.calibration_run == 2:
            suffix = "_validation"
        if self.calibration_run != 2:
            self.file_list = []
            os.system("mkdir "+self.folder_name)
        with open(self.folder_name+f"/IV_raw{suffix}.csv", "w") as f:
            f.write("time [s],Current [uA],Bias mon [V], Bias set [V]\n")
            for entry in self.raw_data:
                line = ""
                for x in entry:
                    line+=f"{x},"
                f.write(line[:-1]+"\n")
        
        with open(self.folder_name+f"/IV_curve{suffix}.json","w") as f:
            json.dump(self.processed_data,f)
        self.file_list.append(f"IV_raw{suffix}.csv")
        self.file_list.append(f"IV_curve{suffix}.json")

        try:
            import ROOT
            from array import array
            ROOT.gROOT.SetBatch(1)
            x_data  = [data[0] for data in self.processed_data]
            y_data  = [data[1] for data in self.processed_data]
            dy_data = [sqrt(data[2]**2 + (0.002+0.01*data[1])**2) for data in self.processed_data]
            dx_data = [2 for data in self.processed_data]
            gg = ROOT.TGraphErrors(len(x_data), array("f", x_data), array("f", y_data), array("f", dx_data), array("f", dy_data))
            ff = None
            if "IV_curve" in os.listdir(self.folder_name):
                ff = ROOT.TFile(self.folder_name+"/IV_curve.root","UPDATE")
            else:
                ff = ROOT.TFile(self.folder_name+"/IV_curve.root","RECREATE")
            canvas = ROOT.TCanvas("IV","IV", 800,800)
            canvas.SetLeftMargin(0.15)
            gg.SetMarkerStyle(20)
            gg.SetLineColor(ROOT.kBlue+1)
            gg.GetXaxis().SetTitle("Bias [V]")
            gg.GetYaxis().SetTitle("Current [#muA]")
            gg.SetTitle("")
            gg.Draw("AP")
            latex = ROOT.TLatex()
            latex.SetNDC()
            latex.SetTextAngle(0)
            latex.SetTextColor(ROOT.kBlack)    

            latex.SetTextFont(42)
            latex.SetTextAlign(11) 
            latex.SetTextSize(0.04)    
            latex.DrawLatex(canvas.GetLeftMargin(),1-canvas.GetTopMargin()+0.01,"CMS Internal")
            latex.SetTextAlign(31) 
            latex.SetTextSize(0.03)    
            latex.DrawLatex(1-canvas.GetRightMargin(),1-canvas.GetTopMargin()+0.01,"Module test bench")
            
            if self.calibration_run == 1:
                self.current_correction = ROOT.TF1("fitf","[0]+[1]*x+[2]*x*x",0,800,3)
                self.current_correction.SetParameters(0,0)
                self.current_correction.SetParameters(1,0)
                self.current_correction.SetParameters(2,1e-8)
                self.current_correction.SetParLimits(2,-1e-7,1e-7)
                self.current_correction.SetParLimits(1,-0.01,0.01)
                self.current_correction.SetParLimits(0,-0.3,0.3)
                gg.Fit(self.current_correction)
                calibration_name = datetime.now().strftime("%y%m%d_%H%M")+"_pol2"
                calibration_values=[self.offset_voltage, self.current_correction.GetParameter(0), self.current_correction.GetParameter(1), self.current_correction.GetParameter(2)]
                print("NEW CALIBRATION GENERATED!!! VALUES:")
                print(calibration_values)
                #Write calibration to json...

                #### ---> Getting overall config
                overall_conf = json.load(open(os.environ["IIHE_TOOLS_BASE_DIR"]+"/data/HV_calib.json", "r"))
                overall_conf["selected_calib"] = calibration_name
                with open(os.environ["IIHE_TOOLS_BASE_DIR"]+"/data/HV_calib.json", "w")  as f:
                    f.write(json.dumps(overall_conf, indent=2))

                calib_conf = json.load(open(os.environ["IIHE_TOOLS_BASE_DIR"]+"/data/HV_calib/"+overall_conf["selected_file"], "r"))
                calib_conf["selected"] = calibration_name
                calib_conf["calibrations"][calibration_name] = calibration_values
                with open(os.environ["IIHE_TOOLS_BASE_DIR"]+"/data/HV_calib/"+overall_conf["selected_file"], "w")  as f:
                    f.write(json.dumps(calib_conf, indent=2))
                #print(calibration_name,calibration_values)
                print("Done updating calibration! Name = ",calibration_name)

                self.request(f"HV load_calib {overall_conf['selected_file']} {calibration_name}")

                self.current_correction_name = f"{self.current_correction.GetParameter(0):.2f} + {1000*self.current_correction.GetParameter(1):.2f} kV + {self.current_correction.GetParameter(2)*1e6:.2f} kV^2"
                
            gg.Write("IV_curve"+suffix)
                
            canvas.Write("IV_curve_canvas"+suffix)
            canvas.Print(self.folder_name+f"/IV_curve{suffix}.png")
            canvas.Print(self.folder_name+f"/IV_curve{suffix}.pdf")

            if self.calibration_run==2:
                hist = ROOT.TH1F("Validation_error","Validation_error",201,-0.01005,0.01005)
                for x in gg.GetY():
                    hist.Fill(x)
                hist.Write("Validation_error")
                hist.Draw()
                canvas.Print(self.folder_name+"/Validation_error.png")
                canvas.Print(self.folder_name+"/Validation_error.pdf")
                self.file_list.append("Validation_error.png")
                self.file_list.append("Validation_error.pdf")

            ff.Close()
            self.file_list.append(f"IV_curve{suffix}.png")
            self.file_list.append(f"IV_curve{suffix}.pdf")
            self.file_list.append(f"IV_curve.root")
            print("Done")
                

        except Exception as e:
            print(e)
        os.system(f"tar -czvf {self.folder_name}.tar.gz {self.folder_name}")
        
        self.file_list = [self.folder_name.split("/")[-1]+"/"+f.replace(self.folder_name.split("/")[-1],"") for f in self.file_list]
        self.file_list.append(self.folder_name.split("/")[-1]+".tar.gz")

    def get_files(self):
        try:
            return self.file_list
        except Exception as e:
            return []

    def load_iv(self):
        pass


if __name__ == "__main__":
    # ir = IV_runner()
    # ir.run_state_machine()
    # print(ir.get_state())
    # time.sleep(1)
    # print(ir.get_state())
    # time.sleep(1)
    # print(ir.get_state())
    # time.sleep(1)

    # ir.start_IV(50,10,1)
    # for i in range(100):
    #     time.sleep(1)
    #     print(ir.processed_data)
    ir = IV_runner()
    ir.run_state_machine()
    time.sleep(1)
    ir.start_IV(0,50,20,0.1, calibration = True)
    # ir.store_iv()
import pyvisa, sys
from pyvisa.constants import StopBits, Parity
import serial.tools.list_ports

BLACK_LIST = []
DEBUG = False

def get_tty(name):
    ports = serial.tools.list_ports.comports()
    dev = []
    for port, desc, hwid in sorted(ports):
        if name in desc:
            dev.append(port)
    return dev

def print_devices():
    ports = serial.tools.list_ports.comports()
    dev = []
    for port, desc, hwid in sorted(ports):
        print(f"Port : {port}; Desc : {desc}; hwid : {hwid}")

def list_devices(silent = False):
    rm = pyvisa.ResourceManager('@py')
    inst_list = rm.list_resources()
    device_list = []
    print(inst_list)
    for inst in inst_list:

        if inst in BLACK_LIST:
            if not silent:
                print(f"Skipping {inst}")
            continue
        if not "/dev/ttyACM" in inst:
            #We are connected in USB, ignore everything else.
            continue

        if not silent:
            print(f"Trying to connect to {inst}...")
        device = rm.open_resource(inst,baud_rate = 57600, data_bits=8, stop_bits=StopBits.one, parity=Parity.odd)
        try:
            if not silent:
                print("Query 'IDN' ...   ", end="")
                sys.stdout.flush()
            answer = device.query("*IDN?")
            fab,mod,ser = answer.split(",")[:3]
            device_list.append([inst,fab,mod,ser])
            if not silent:
                print(f"-- SUCCESS ! Added device {inst} ({mod} PSU)")
            continue
        except Exception as e:
            if not silent:
                print("-- invalid")
            if DEBUG:
                print(e)

        try:
            if not silent:
                print("Query 'BDNAME' ...", end="")
                sys.stdout.flush()
            print("Getting name...")
            answer = device.query("$CMD:MON,PAR:BDNAME\r")
            print(answer)
            assert "#CMD:OK,VAL:DT5471 3kV / 500uA" in answer
            fab,mod,ser = ("CAEN", "DT5471", "0")
            device_list.append([inst,fab,mod,ser])
            if not silent:
                print(f"-- SUCCESS ! Added device {inst} ({mod} PSU)")
        except Exception as e:
            if not silent:
                print("-- invalid")
            if DEBUG:
                print(e)

    if not silent:
        print(f"Found {len(device_list)} devices:")
        for dev in device_list:        
            print(dev)
    return device_list


def get_device(fab = None, mod = None, ser = None):
    dev_list = list_devices(silent = True) 
    dev_filtered = []
    for dev in dev_list:
        if fab != None and not fab in dev[1]:
            continue
        if mod != None and not mod in dev[2]:
            continue
        if ser != None and not ser in dev[3]:
            continue
        dev_filtered.append(dev)
    if len(dev_filtered) == 0:
        print(f"No devices found.")
        return None
    elif len(dev_filtered) > 1 : 
        print(f"Warning ! {len(dev_filtered)} devices found. Reading first one.")
    rm = pyvisa.ResourceManager('@py')
    return rm.open_resource(dev_filtered[0][0],baud_rate = 57600, data_bits=8, stop_bits=StopBits.one, parity=Parity.odd)

def open_device(tty):
    rm = pyvisa.ResourceManager('@py')
    if "USB" in tty:
        return rm.open_resource(tty)
    else:
        return rm.open_resource(tty,baud_rate = 57600, data_bits=8, stop_bits=StopBits.one, parity=Parity.odd)

if __name__=="__main__":
    print_devices()
    list_devices()

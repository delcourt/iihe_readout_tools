import os, json

calib_dir = os.environ["IIHE_TOOLS_BASE_DIR"]+"/data/HV_calib/"

class calibrated_psu:
    def __init__(self):
        self.calibration = [0,0,0,0]    #
        #Vcal = V + V0
        #Ical = I + I0 + I1*Vcal + I2*Vcal**2
        self.calibration_string = "Not calibrated"

    # def load_calibration_file(self, file_name):
    #     try:
    #         self.settings = json.load(open(calib_dir+file_name, "r"))
    #         print("Debug:")
    #         print(self.settings)

    #         self.calibration = [float(vv) for vv in self.settings["calibrations"][self.settings["selected"]]]
    #         return 1
    #     except Exception as e:
    #         print(f"Error: {e}")
    #         return 0

    # def write_calibration_file(self,file_name):
    #     try:
    #         with open(calib_dir+file_name, "w") as outfile:
    #             outfile.write(json.dumps(self.settings, indent=2))
    #         return 1
    #     except Exception as e:
    #         print(f"Error: {e}")
    #         return 0

    
    # def get_calib_string(self):
    #     if len(self.calib_params_voltage) == 0:
    #         return "None"
    #     elif len(self.calib_params_current) == 1:
    #         return "Offset"
    #     elif len(self.calib_params_current) == 3:
    #         return f"{self.calib_params_current[0]:.2f} + {self.calib_params_current[1]:.2f} V + {self.calib_params_current[2]:.2f} V^2"
    #     else:
    #         return "Undefined"

    
    def apply_calib_v(self,v_raw):
        return v_raw - self.calibration[0]
    
    def apply_calib_i(self,i_raw, v_raw):
        vcal = self.apply_calib_v(v_raw)
        (V0, i0, i1, i2) = self.calibration
        return i_raw - i0 - i1*vcal - i2*(vcal**2)

    # def set_calib(self, calib_name):
    #     try:
    #         self.settings["selected"] = calib_name
    #         print("Debug:")
    #         print(self.settings)
    #         self.calibration = [float(vv) for vv in self.settings["calibrations"][calib_name]]
    #         return 1
    #     except Exception as e:
    #         print(f"Error: {e}")
    #         return 0


    # def add_calib(self, calib_name, *vals):
    #     if len(vals) != 4:
    #         print("Error, we need 4 values for the calibration")
    #         return 0
    #     try:
    #         self.settings["selected"] = calib_name
    #         self.settings["calibrations"][calib_name] = [float(vv) for vv in vals] #Warning, not checking if we are overriding anything
    #         self.calibration = [float(vv) for vv in self.settings["calibrations"][calib_name]]
    #         return 1
    #     except Exception as e:
    #         print(f"Error: {e}")
    #         return 0

    def set_calib(self, *vals):
        if len(vals) != 4:
            print("Error, we need 4 values for the calibration")
            return 0
        self.calibration =  [float(vv) for vv in vals]
        self._update_calib_string()
        return 1
    
    def _update_calib_string(self):
        if self.calibration == [0,0,0,0]:
            self.calibration_string = "Uncalibrated"
        elif self.calibration[2] == 0 and self.calibration[3] == 0:
            self.calibration_string =  f"Offset correction. Vo =  {self.calibration[0]:.1f}V, Io = {self.calibration[1]:.3f}µA"
        else:
            self.calibration_string =  f"Pol2 correction. Vo = {self.calibration[0]:.1f}V, I = {self.calibration[1]:.2f} + {1000*self.calibration[2]:.2f} kV + {self.calibration[3]*1e6:.2f} kV^2"

    def load_calib(self, calib_file,calib_name):
        try:
            settings = json.load(open(calib_dir+calib_file, "r"))

            self.calibration = [float(vv) for vv in settings["calibrations"][calib_name]]
            self._update_calib_string()
            return 1
        except Exception as e:
            print(f"Error: {e}")
            return 0

    def get_calib_string(self):
        try:
            return self.calibration_string
        except Exception as e:
            return "Unkown calibration"

if __name__ == "__main__":
    cpsu = calibrated_psu()
    # cpsu.settings = {"selected": "None", "calibrations": {"None":[0,0,0,0]}}
    # cpsu.write_settings(os.environ["IIHE_TOOLS_BASE_DIR"]+"/data/HV_calib.json")
    cpsu.load_calibration_file("HV_calib.json")
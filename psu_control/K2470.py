
import pyvisa
import psu_common
import time,os,json
import threading
VOLT_LIMITS = [0,900]

RAMP_STEP  = 5  #5 volts per ramp step
RAMP_SPEED_DEFAULT_DOWN = 20 #
RAMP_SPEED_DEFAULT_UP   = 20

class K2470:
    def __init__(self,tty, serial_number = None):
        self.inst = psu_common.open_device(tty)
        self.tty = tty
        self.V0 = 0
        self.I0 = 0
        self._ramp_down = RAMP_SPEED_DEFAULT_DOWN
        self._ramp_up   = RAMP_SPEED_DEFAULT_UP
        self.ramp_status   = False
        self._stop_ramp = False
        self.thread = None
        self._communication_lock = threading.Lock()
        self.write("*CLS")
        # print(self.query("*ESR"))
        if self.inst == None:
            print("Fatal error, unable to connect to PSU !")
        if not "keithley" in self.get_id().lower():
            print("The device isn't responding as a Keithley:")
            print(self.get_id())
        self.set_i_compliance(1e-5)

    def claimed_port(self):
        return self.tty
    
    def ramp(self):
        v_set = self.get_v_set()
        while v_set != self._v_target and self._stop_ramp == False:
            dv = v_set
            if v_set < self._v_target:
                self.ramp_status = "RAMP_UP"
                v_set += RAMP_STEP
                if v_set > self._v_target:
                    v_set = self._v_target
                dv -= v_set
                dv = abs(dv)
                self.set_v_no_ramp(v_set)
                time.sleep(dv/self._ramp_down)
            else:
                self.ramp_status = "RAMP_DOWN"
                v_set -= RAMP_STEP
                if v_set < self._v_target:
                    v_set = self._v_target
                dv -= v_set
                dv = abs(dv)
                self.set_v_no_ramp(v_set)
                time.sleep(dv/self._ramp_down)
        print("Ramp done!")
        self.ramp_status = "OK"
        self._stop_ramp = False


    def set_v(self, v_target, channel = 0):
        self.channel_check(channel)
        print(f"Starting ramp to {v_target}", end = "")
        self._v_target = v_target
        if self.thread == None or self.ramp_status == "OK":
            self.thread = threading.Thread(target=self.ramp)
            self.thread.start()
        print(" Done !")

    def query(self,command):
        with self._communication_lock:
            r_str=self.inst.query(command).replace("\x11","").replace("\r","").replace("\n","").replace("\x13","")
        r_str = r_str.split("\n")
        if len(r_str)==0:
            return("ERR : No answer from device")
        elif len(r_str) == 1:
            return(r_str[0])
        else:
            print(f"ERR : answer from device is following: {[xx for xx in r_str]}")

    def write(self,command):
        with self._communication_lock:
            self.inst.write(command)
        return 1

    def get_id(self):
        return(self.query("*IDN?"))

    def channel_check(self,channel):
        if channel != 0:
            print(f"Warning, trying to access unexisting channel {channel}")

    def get_voltage(self, channel = 0):
        self.channel_check(channel)
        return float(self.query(":meas:volt?"))

    def get_current(self, channel = 0):
        self.channel_check(channel)
        return(self.query(":meas:current?"))

    def get_measurements(self):
        meas = {}
        meas[f"HV"] = float(self.get_voltage())
        meas[f"HI"] = float(self.get_current())
        return json.dumps(meas)


    def allow_local(self):
        return("ERR : This command isn't possible on this PSU")

    def set_local(self):
        return("ERR : This command isn't possible on this PSU")

    def is_enabled(self,channel = 0):
        self.channel_check(channel)
        return self.query("OUTP?")

    def get_status_list(self,channel = 0):
        pass
        # self.channel_check(channel)
        # stat = self.get_status()
        # stl = []
        # if stat %2 == 1:
        #     stl.append("ON")
        # else:
        #     stl.append("OFF")
        # messages = ["ON", "RAMP UP", "RAMP DOWN", "OVER CURRENT", "OVER VOLTAGE", "UNDER VOLTAGE", "MAXV PROTECTION", "TRIP", "OVER TEMP", "res", "DISABLED", "KILL", "INTERLOCK", "CALIB ERROR"]
        # for bb in range(1,14):
        #     if (stat >>bb)%2 ==1:
        #         stl.append(messages[bb])
        # return stl




    def get_status(self,channel = 0):
        pass
        # self.channel_check(channel)
        # stat = int(self.query("$CMD:MON,PAR:STAT\r"))
        # return stat


    def turn_on(self, channel = 0):
        self.channel_check(channel)
        return self.write(":OUTP:STAT 1")

    def turn_off(self, channel = 0):
        self.channel_check(channel)
        return self.write(":OUTP:STAT 0")

    def get_v_set(self, channel = 0):
        self.channel_check(channel)
        return(float(self.query(":source:voltage?")))

    def set_v_no_ramp(self, volt, channel = 0):
        self.channel_check(channel)
        print(f"Setting voltage to {volt}")
        volt = float(volt)
        if volt < VOLT_LIMITS[0] or volt > VOLT_LIMITS[1]:
            raise "Voltage ({volt}) out of bounds !"
        return (self.write(f":source:voltage {volt}"))

    def set_i_compliance(self, current, channel = 0):
        self.channel_check(channel)
        current = float(current)
        return (self.write(f":SOUR:VOLT:ILIMit {current}"))

    def get_i_compliance(self, channel = 0):
        self.channel_check(channel)
        return (self.query(f"SOUR:VOLT:ILIMit?"))

    def set_ramp_up(self,volt, channel = 0):
        pass
        # self.channel_check(channel)
        # return(self.query(f"$CMD:SET,PAR:RUP,VAL:{volt}\r"))

    def set_ramp_down(self,volt, channel = 0):
        pass
        # self.channel_check(channel)
        # return(self.query(f"$CMD:MON,PAR:RUP\r"))

    def get_ramp_down(self, channel = 0):
        pass
        # self.channel_check(channel)
        # return(self.query(f"$CMD:MON,PAR:RDW\r"))

    def clear_error(self,channel = 0):
        pass
        # self.channel_check(channel)
        # return(self.query(f"$CMD:SET,PAR:BDCLR\r"))
    def get_language(self):
        return self.query("*LANG?")

    def reset(self):
        return self.write("*RST")

if __name__=="__main__":
    # psu_common.list_devices()
    hv_psu = K2470(tty="USB0::1510::9328::04603204::0::INSTR")
    # hv_psu.write(":SOUR:VOLT:PROT:NONE")
    # print(hv_psu.query("SOUR:VOLT:PROT?"))
    # print(hv_psu.write(":SOURce:VOLTage:RANGe 800"))
    print(hv_psu.query("TRIGger:CONTinuous?"))
    print("Delay:",hv_psu.query(":SOUR:CURR:DEL:AUTO?"))
    # print(hv_psu.get_language())
    # print(hv_psu.get_id())
    # hv_psu.reset()
    # hv_psu.set_v(10)
    # print(f"Is enabled: {hv_psu.is_enabled()}, voltage is {hv_psu.get_voltage()}, current is {hv_psu.get_current()} and v_set is {hv_psu.get_v_set()}")
    # hv_psu.turn_on()
    # print(f"Is enabled: {hv_psu.is_enabled()}, voltage is {hv_psu.get_voltage()}, current is {hv_psu.get_current()} and v_set is {hv_psu.get_v_set()}")
    # hv_psu.turn_off()
    # print(f"Is enabled: {hv_psu.is_enabled()}, voltage is {hv_psu.get_voltage()}, current is {hv_psu.get_current()} and v_set is {hv_psu.get_v_set()}")

    # print(hv_psu.get_measurements())
    # print(hv_psu.set_i_compliance(10e-6))
    # print(hv_psu.get_i_compliance())
    print(hv_psu.turn_off())
    
    print(hv_psu.set_v(0))
    # print(hv_psu.write(":SOUR:SWE:VOLT:LIN 0, 100, 100"))
    # print(hv_psu.write("INIT"))
    for i in range(100):
        # print(f"Is enabled: {hv_psu.is_enabled()}, voltage is {hv_psu.get_voltage()}, current is {hv_psu.get_current()} and v_set is {hv_psu.get_v_set()}")
        print(f"Is enabled: {hv_psu.is_enabled()} and v_set is {hv_psu.get_v_set()}, ramp = {hv_psu.ramp_status}")
        time.sleep(0.5)
        
